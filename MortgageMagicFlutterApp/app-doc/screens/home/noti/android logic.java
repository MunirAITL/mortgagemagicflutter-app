override fun showNotificationDetailsActivity(notifications: Notifications) {
// if (notifications.EntityName == AppConstants.NOTIFICATION_NAME_Task) {
// val intent = Intent(applicationContext, TaskDetailsActivity::class.java)
// intent.putExtra("PUSHNOTIFICATION_TASKID", notifications.EntityId!!)
// startActivity(intent)
// } else if (notifications.EntityName == AppConstants.NOTIFICATION_NAME_TaskBidding) {
// val intent = Intent(applicationContext, OfferConversationActivity::class.java)
// intent.putExtra("PUSHNOTIFICATION_TASKID", notifications.EntityId!!)
// startActivity(intent)
// }


if (notifications.EntityName == AppConstants.NOTIFICATION_NAME_Task) {

	intentTemporary = Intent(this, TaskPostByWebViewActivity::class.java)
	intent!!.putExtra("PUSHNOTIFICATION_TASKID", notifications.EntityId!!)
	intent!!.putExtra("PUSHNOTIFICATION_MESSAGE", notifications.Message!!)
	intent!!.putExtra("PUSHNOTIFICATION_WEBURL", notifications.WebUrl!!)

} else if (notifications.EntityName == AppConstants.NOTIFICATION_NAME_TaskBidding) {
	notifications.Description = getString(R.string.push_notification_make_offer, notifications.InitiatorDisplayName)
	intentTemporary = Intent(this, OfferConversationActivity::class.java)
	intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKBIDDINGID", notifications.EntityId!!)
} else if (notifications.EntityName == AppConstants.NOTIFICATION_NAME_TaskBiddingComment || notifications.EntityName == AppConstants.NOTIFICATION_NAME_TaskBiddingAdminComment) {

	notifications.Description = getString(R.string.push_notification_task_comment, notifications.InitiatorDisplayName)

	// notifications.Description = getString(R.string.push_notification_task_comment, notifications.InitiatorDisplayName)
when {
	notifications.NotificationEventId == 1011
	-> {

		notifications.Description = getString(R.string.push_notification_private_message, notifications.InitiatorDisplayName)
		intentTemporary = Intent(this, ChatActivity::class.java)
		intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKID", notifications.EntityId!!)
	}
		notifications.NotificationEventId == 1012
		-> {

			notifications.Description = getString(R.string.push_notification_offer_message, notifications.InitiatorDisplayName)
			intentTemporary = Intent(this, OfferConversationActivity::class.java)
			intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKBIDDINGID", notifications.EntityId!!)

		}
		else -> {
			intentTemporary = Intent(this, PublicChatActivity::class.java)
			intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKID", notifications.EntityId!!)
		}
	}
} else if (notifications.EntityName == AppConstants.NOTIFICATION_NAME_TaskBiddingRequestPayment) {

// notifications.Description = getString(R.string.push_notification_payment_request, notifications.InitiatorDisplayName )
//
// intentTemporary = Intent(this, TaskNotificationActivity::class.java)
// intentTemporary!!.putExtra("title", notifications!!.Message)
// intentTemporary!!.putExtra("body", notifications.Description)

	notifications.Description = getString(R.string.push_notification_private_message, notifications.InitiatorDisplayName)
	intentTemporary = Intent(this, ChatActivity::class.java)
	intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKID", notifications.EntityId!!)

//intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKID", notifications!!.Message)
} 
else if (notifications.EntityName == AppConstants.NOTIFICATION_NAME_TaskBiddingAccepted) {

	notifications.Description = getString(R.string.push_notification_accept_offer, notifications.InitiatorDisplayName)

	intentTemporary = Intent(this, OfferConversationActivity::class.java)
	intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKBIDDINGID", notifications.EntityId!!)

	//intentTemporary = Intent(this, TaskNotificationActivity::class.java)
	intentTemporary!!.putExtra("title", notifications!!.Message)
	intentTemporary!!.putExtra("body", notifications.Description)


	//intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKID", notifications!!.Message)
} else if (notifications != null && notifications?.EntityName == AppConstants.NOTIFICATION_NAME_TaskPaymentConfirmation) {


	notifications.Description = getString(R.string.push_notification_payment_confirmation, notifications.InitiatorDisplayName)

	intentTemporary = Intent(this, OfferConversationActivity::class.java)
	intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKBIDDINGID", notifications.EntityId!!)

	intentTemporary!!.putExtra("title", notifications!!.Message)
	intentTemporary!!.putExtra("body", notifications.Description)

} else if (notifications != null && notifications?.EntityName == AppConstants.NOTIFICATION_NAME_UserRating) {

	intentTemporary = Intent(this, TaskPostByWebViewActivity::class.java)
	intentTemporary!!.putExtra("PUSHNOTIFICATION_TASKID", notifications.Description!!.toLong()!!)
	intent!!.putExtra("PUSHNOTIFICATION_MESSAGE", notifications.Message!!)
	intent!!.putExtra("PUSHNOTIFICATION_WEBURL", notifications.WebUrl!!)

	notifications.Description = getString(R.string.push_notification_user_review, notifications.InitiatorDisplayName)

	intentTemporary!!.putExtra("title", notifications!!.Message)
	intentTemporary!!.putExtra("body", notifications.Description)

}
else if (notifications != null && notifications?.EntityName == AppConstants.NOTIFICATION_NAME_Resolution) {
	TaskManager.task = null
	intentTemporary = Intent(this, ChatActivity::class.java)

}
else {
	intentTemporary = Intent(this, TaskNotificationActivity::class.java)
	intentTemporary!!.putExtra("title", notifications!!.Message)
	intentTemporary!!.putExtra("body", notifications.Description)
	intentTemporary!!.putExtra("weburl", notifications.WebUrl)
	intentTemporary!!.putExtra("notificationId", notifications.Id)
}

startActivity(intentTemporary)
}