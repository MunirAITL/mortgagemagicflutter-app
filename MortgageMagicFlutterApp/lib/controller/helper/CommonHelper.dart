import 'package:Mortgage_Magic/view/mywidgets/IcoTxtIco.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class CommonHelper with Mixin {
  getLine({BuildContext context}) {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: Container(width: getW(context), height: 0.5, color: Colors.black),
    );
  }

  getDOB({BuildContext context, String dob, Function callback}) {
    DateTime date = DateTime.now();
    var newDate = new DateTime(date.year - 18, date.month, date.day);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Provide a date of birth",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
          ),
          SizedBox(height: 5),
          GestureDetector(
            onTap: () {
              /*
                initialDate : Default Selected Date In Picker Dialog
                firstDate : From Minimum Date Of Your Date Picker
                lastDate : Max Date Of To-Date Of Date Picker
              */
              showDatePicker(
                context: context,
                initialDate: newDate,
                firstDate: DateTime(1950),
                lastDate: newDate,
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light(), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: IcoTxtIco(
                leftIcon: Icons.calendar_today,
                txt: (dob == "") ? "Select a date" : dob,
                rightIcon: Icons.arrow_drop_down,
                txtAlign: TextAlign.left,
                rightIconSize: 50,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
