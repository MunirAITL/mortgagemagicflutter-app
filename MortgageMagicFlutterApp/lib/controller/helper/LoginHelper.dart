class LoginHelper {
  getParam({
    email,
    pwd,
    persist = false,
    checkSignUpMobileNumber = false,
    countryCode = "880",
    status = "101",
    oTPCode = "",
    birthDay = "",
    birthMonth = "",
    birthYear = "",
    userCompanyId = "0",
  }) {
    return {
      "Email": email,
      "Password": pwd,
      "Persist": false,
      "CheckSignUpMobileNumber": false,
      "CountryCode": "880",
      "Status": "101",
      "OTPCode": "",
      "BirthDay": "",
      "BirthMonth": "",
      "BirthYear": "",
      "UserCompanyId": "0"
    };
  }
}
