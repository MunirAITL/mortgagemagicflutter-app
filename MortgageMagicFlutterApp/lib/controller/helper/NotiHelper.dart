import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';

import 'package:Mortgage_Magic/view/dashboard/webview/WebScreen.dart';
import 'package:Mortgage_Magic/config/dashboard/NotiCfg.dart';
import 'package:Mortgage_Magic/model/json/NotiModel.dart';
import 'package:Mortgage_Magic/view/dashboard/msg/chat/GroupChatScreen.dart';
import 'package:Mortgage_Magic/view/dashboard/msg/chat/TaskBiddingScreen.dart';
import 'package:Mortgage_Magic/view/dashboard/noti/details/NotiDetailsScreen.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class NotiHelper with Mixin {
  getUrl({pageStart, pageCount, caseStatus, UserModel userModel}) {
    var url = Server.NOTI_URL;
    url =
        url.replaceAll("#userId#", userModel.userCompanyInfo.userID.toString());
    return url;
  }

  Map<String, dynamic> getNotiMap({NotiModel model, UserModel userModel}) {
    Map<String, dynamic> notiMap = {};
    try {
      //print(NotiCfg.EVENT_LIST.toString());
      for (var map in NotiCfg.EVENT_LIST) {
        final int listCommunityId = map["communityId"];
        int notificationEventId = map['notificationEventId'];
        if (listCommunityId == int.parse(userModel.communityID)) {
          if (model.entityName == map["entityName"] &&
              (model.notificationEventId == notificationEventId ||
                  notificationEventId == 0)) {
            String txt = map["txt"];
            txt = txt.replaceAll(
                "#InitiatorDisplayName#", model.initiatorDisplayName);
            txt = txt.replaceAll("#EventName#", model.eventName);
            txt = txt.replaceAll('[] ', '');

            //  text processing
            notiMap['txt'] = txt ?? '';
            notiMap['publishDateTime'] = getTimeAgoTxt(model.publishDateTime);

            //  event processing
            //notiMap['communityId'] = map['communityId'];
            //notiMap['desc'] = map['desc'];
            //notiMap['notificationEventId'] = map['notificationEventId'];
            notiMap['route'] =
                (map['route2'] == null) ? map['route'] : map['route2'];

            break;
          }
        }
      }
    } catch (e) {
      print(e.toString());
    }
    return notiMap;
  }

  setRoute({
    BuildContext context,
    UserModel userModel,
    NotiModel model,
    Map<String, dynamic> notiMap,
    Function callback,
  }) async {
    try {
      Type route = notiMap['route'];
      if (identical(route, WebScreen)) {
        await Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (__) => WebScreen(
                      title: model.message,
                      url: Server.BASE_URL_NOTI_WEB + model.webUrl,
                    )));
      } else if (identical(route, NotiDetailsScreen)) {
        await Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (__) => NotiDetailsScreen(
                      model: model,
                      notiMap: notiMap,
                      userModel: userModel,
                    )));
      } else if (identical(route, TaskBiddingScreen)) {
        await Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (__) => TaskBiddingScreen(
                      title: model.eventName,
                      taskId: model.entityId,
                    )));
      } else if (identical(route, GroupChatScreen)) {
        await Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (__) => GroupChatScreen(
                      model: model,
                      notiMap: notiMap,
                      userModel: userModel,
                    )));
      } else {
        //if (identical(route, MsgTab)) {
        callback();
        //Navigator.pop(context);
      }
    } catch (e) {}
  }
}

/*extension IndexedIterable<E> on Iterable<E> {
  Iterable<T> mapIndexed<T>(T Function(E e, int i) f) {
    var i = 0;
    return map((e) => f(e, i++));
  }
}*/
