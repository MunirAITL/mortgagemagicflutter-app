import 'package:Mortgage_Magic/config/Server.dart';

class TimeLineHelper {
  getUrl({
    pageStart,
    pageCount,
    taskId,
    isPrivate,
    customerId,
    senderId,
    receiverId,
    timeLineId,
  }) {
    var url = Server.TIMELINE_URL;
    url = url.replaceAll("#isPrivate#", isPrivate.toString());
    url = url.replaceAll("#receiverId#", receiverId.toString());
    url = url.replaceAll("#senderId#", senderId.toString());
    url = url.replaceAll("#taskId#", taskId.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#customerId#", customerId.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#timeLineId#", timeLineId.toString());
    return url;
  }
}
