import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/dashboard/more/help/HelpScreen.dart';
import 'package:Mortgage_Magic/view/dashboard/more/profile/ProfileScreen.dart';
import 'package:Mortgage_Magic/view/dashboard/more/reviews/ReviewsScreen.dart';
import 'package:Mortgage_Magic/view/dashboard/more/settings/SettingsScreen.dart';
import 'package:Mortgage_Magic/view/dashboard/noti/NotiTab.dart';
import 'package:flutter/material.dart';

class MoreHelper {
  static const List<Map<String, dynamic>> listMore = [
    {"title": "Profile", "route": ProfileScreen},
    //{"title": "Reviews", "route": ReviewsScreen},
    {"title": "Notifications", "route": NotiTab},
    {"title": "Settings", "route": SettingsScreen},
    {"title": "Help", "route": HelpScreen},
    {"title": "Logout", "route": null},
  ];

  setRoute({
    Type route,
    BuildContext context,
    Function callback,
  }) async {
    try {
      if (identical(route, ProfileScreen)) {
        await Navigator.push(context,
                new MaterialPageRoute(builder: (__) => ProfileScreen()))
            .then((value) => callback(route));
      } else if (identical(route, ReviewsScreen)) {
        await Navigator.push(context,
                new MaterialPageRoute(builder: (__) => ReviewsScreen()))
            .then((value) => {callback(route)});
      } else if (identical(route, SettingsScreen)) {
        await Navigator.push(context,
                new MaterialPageRoute(builder: (__) => SettingsScreen()))
            .then((value) => {callback(route)});
      } else if (identical(route, HelpScreen)) {
        await Navigator.push(
                context, new MaterialPageRoute(builder: (__) => HelpScreen()))
            .then((value) => {callback(route)});
      }
    } catch (e) {}
  }
}
