import 'dart:io';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mypkg/model/data/PrefMgr.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:polygon_clipper/polygon_border.dart';
import 'package:polygon_clipper/polygon_clipper.dart';

class ProfileHelper with Mixin {
  static const String ProfilePicFG_Key = "ProfilePicFG";
  static const String ProfilePicBG_Key = "ProfilePicBG";

  List<String> _items = ['Gallery', 'Camera', 'Skip'];

  getUserOnlineStatus({BuildContext context, int statusIndex = 1}) {
    Color colr = Colors.greenAccent.shade400;
    String status = "Online"; //  statusIndex=1
    if (statusIndex == 0) {
      status = "Offline";
      colr = Colors.redAccent.shade400;
    } else if (statusIndex == 2) {
      status = "Away";
      colr = Colors.yellowAccent.shade400;
    }

    return Container(
      //width: getWP(context, 50),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: getWP(context, 5),
            height: getWP(context, 5),
            decoration: BoxDecoration(shape: BoxShape.circle, color: colr),
          ),
          SizedBox(width: 10),
          Txt(
              txt: status,
              txtColor: Colors.white,
              txtSize: 2.5,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getStarsRow(int rate, Color colr) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            Icons.star,
            color: (i < rate) ? colr : Colors.grey,
            size: 20,
          ),
      ],
    );
  }

  getStarRatingView({int rate, int reviews}) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          getStarsRow(rate, MyTheme.themeData.accentColor),
          SizedBox(width: 10),
          Txt(
              txt: reviews.toString() + " Reviews",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getCompletionText({int pa}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Txt(
              txt: pa.toString() + "% Completion Rate",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.center,
              isBold: false),
          IconButton(
            icon: Icon(
              Icons.info,
              color: Colors.grey, //MyTheme.themeData.accentColor,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  getAvatorStarView({int rate}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(18.0),
            margin: EdgeInsets.only(right: 16.0),
            decoration: ShapeDecoration(
                shape: PolygonBorder(
                    sides: 6,
                    borderRadius: 1,
                    border: BorderSide(color: Colors.grey, width: 0.5))),
            child: Image.asset(
              "assets/images/icons/user_icon.png",
              width: 40,
              height: 40,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              getStarsRow(rate, Colors.cyan),
              SizedBox(height: 10),
              Txt(
                  txt: getTimeAgoTxt("2021-01-27T16:16:33.47Z"),
                  txtColor: Colors.black,
                  txtSize: 2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          )
        ],
      ),
    );
  }

  //  ********* CAM AND GALLERY Stuff Start Here...

  Future<File> _openCamera({isRear = true, String key}) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      preferredCameraDevice: (isRear) ? CameraDevice.rear : CameraDevice.front,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  Future<File> _openGallery({String key}) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    if (pickedFile != null) {
      File path = File(pickedFile.path);
      await PrefMgr.shared.setPrefStr(key, pickedFile.path);
      return path;
    }
  }

  void showCamModal(
      {BuildContext context,
      String prefkey,
      Function callback,
      bool isRear = true}) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          color: Colors.white,
          padding: EdgeInsets.all(8),
          height: 160,
          alignment: Alignment.center,
          child: ListView.separated(
              itemCount: _items.length,
              separatorBuilder: (context, int) {
                return Divider();
              },
              itemBuilder: (context, index) {
                return GestureDetector(
                  child: Column(
                    children: [
                      Text(
                        _items[index],
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                      (index == 2) ? SizedBox(height: 50) : SizedBox()
                    ],
                  ),
                  onTap: () async {
                    Navigator.pop(context);
                    switch (index) {
                      case 0:
                        callback(await _openGallery(key: prefkey)); //  gallery
                        break;
                      case 1:
                        callback(await _openCamera(
                          key: prefkey,
                          isRear: isRear,
                        )); //  cam
                        break;
                      default:
                        break;
                    }
                  },
                );
              }),
        );
      },
    );
  }
}
