import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';

class MyCaseHelper {
  getUrl({pageStart, pageCount, caseStatus, UserModel userModel}) {
    var url = Server.NEWCASE_URL;
    url =
        url.replaceAll("#userId#", userModel.userCompanyInfo.userID.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#status#", caseStatus.toString());
    return url;
  }
}
