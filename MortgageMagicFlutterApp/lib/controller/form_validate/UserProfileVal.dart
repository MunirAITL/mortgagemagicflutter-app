import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class UserProfileVal with Mixin {
  static const int PHONE_LIMIT = 8;

  isNotEmpty(TextEditingController tf, arg) {
    if (tf.text.isEmpty) {
      showToast(msg: "Invalid " + arg);
      return false;
    }
    return true;
  }

  isFNameOK(TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(msg: "Invalid first name");
      return false;
    }
    return true;
  }

  isLNameOK(TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(msg: "Invalid last name");
      return false;
    }
    return true;
  }

  isEmailOK(TextEditingController tf) {
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(tf.text.trim())) {
      showToast(msg: "Invalid email address");
      return false;
    }
    return true;
  }

  isPhoneOK(TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(msg: "Invalid Phone");
      return false;
    }
    return true;
  }

  isPwdOK(TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(msg: "Invalid Password");
      return false;
    }
    return true;
  }

  isComNameOK(TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(msg: "Invalid Company Name");
      return false;
    }
    return true;
  }

  isDOBOK(str) {
    if (str == '') {
      showToast(msg: "Invalid Date of Birth");
      return false;
    }
    return true;
  }
}
