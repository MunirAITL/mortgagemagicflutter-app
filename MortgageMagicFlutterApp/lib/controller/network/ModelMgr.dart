import 'package:Mortgage_Magic/model/json/LoginAPIModel.dart';
import 'package:Mortgage_Magic/model/json/MediaUploadFilesAPIModel.dart';
import 'package:Mortgage_Magic/model/json/ResolutionAPIModel.dart';
import 'package:Mortgage_Magic/model/json/TimeLineAPIModel.dart';
import 'package:Mortgage_Magic/model/json/NotiAPIModel.dart';
import 'package:Mortgage_Magic/model/json/PostCaseAPIModel.dart';
import 'package:Mortgage_Magic/model/json/RegAPIModel.dart';
import 'package:Mortgage_Magic/model/json/TaskBiddingAPIModel.dart';
import 'package:Mortgage_Magic/model/json/TaskInfoSearchAPIModel.dart';

//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
class ModelMgr {
  /// If T is a List, K is the subtype of the list.
  Future<T> fromJson<T, K>(dynamic json) async {
    if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseAPIModel)) {
      return PostCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAPIModel)) {
      return TimeLineAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    }
    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
