//  https://github.com/flutterchina/dio
import 'dart:convert';
import 'dart:io';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:connectivity/connectivity.dart';
import 'package:mypkg/Mixin.dart';
import 'package:mypkg/widgets/MyAlert.dart';
import 'ModelMgr.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'CookieMgr.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';

//typedef mapValue = Function(Map<String, dynamic>);

class NetworkMgr with Mixin {
  Dio _dio;

  Future<T> postData<T, K>({context, url, param, xdir = ""}) async {
    if (await hasNetwork()) {
      try {
        startLoading();
        log("ws::postData==================" + url + xdir);
        for (var p in param.entries) {
          log(p.key + '=' + p.value.toString());
        }
        _dio = Dio();
        _dio.options.headers = CookieMgr.headers;
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(Uri.parse(url), cj.loadForRequest(Uri.parse(url)));
        //log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(url, data: param);
        _dio.close();
        log(response.data.toString());
        stopLoading();
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } catch (e) {
        log(e.toString());
        stopLoading();
      }
    } else {
      MyAlert(context, "Sorry, internet is not available", 2, null);
    }
    return null;
  }

  Future<T> getData<T, K>({context, url, isLoading = true}) async {
    if (await hasNetwork()) {
      try {
        if (isLoading) {
          startLoading();
        }
        log("ws::getData==================" + url);
        _dio = Dio();
        _dio.options.headers = CookieMgr.headers;
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(Uri.parse(url), cj.loadForRequest(Uri.parse(url)));
        //log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.get(url);
        _dio.close();
        log(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } catch (e) {
        if (isLoading) {
          stopLoading();
        }
      }
    } else {
      MyAlert(context, "Sorry, internet is not available", 2, null);
    }
    return null;
  }

  //  files only
  Future<T> uploadFiles<T, K>(
      {context, url, List<File> files, isLoading = true}) async {
    try {
      if (await hasNetwork()) {
        if (isLoading) {
          startLoading();
        }
        log("ws::uploadFiles:: files only  ==================" + url);
        var formData = FormData();
        for (var file in files) {
          String mimeStr = lookupMimeType(file.path);
          var fileType = mimeStr.split('/');
          log('file type ${mimeStr}, ${fileType}');
          String fileName = file.path.split('/').last;
          formData.files.addAll([
            MapEntry(
                "file",
                await MultipartFile.fromFile(file.path,
                    filename: fileName,
                    contentType: MediaType(fileType[0], mimeStr)))
          ]);
        }

        _dio = Dio();
        /*_dio.options.headers = {
          'Content-type': 'multipart/form-data',
          'Accept': 'application/json'
        };*/
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(Uri.parse(url), cj.loadForRequest(Uri.parse(url)));
        log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(url, data: formData);
        _dio.close();
        log(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } else {
        MyAlert(context, "Sorry, internet is not available", 0, null);
      }
    } catch (e) {
      if (isLoading) {
        stopLoading();
      }
      showAlertErr(context: context, title: url, msg: e.toString());
    }
  }

  //  multipart
  Future<T> postFile<T, K>({context, url, param, isLoading = true}) async {
    try {
      if (await hasNetwork()) {
        if (isLoading) {
          startLoading();
        }
        log("ws::postFile :: multipart  ==================" + url);
        _dio = Dio();
        _dio.options.headers = CookieMgr.headers;
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(Uri.parse(url), cj.loadForRequest(Uri.parse(url)));
        //log(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(url, data: FormData.fromMap(param));
        _dio.close();
        log(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } else {
        MyAlert(context, "Sorry, internet is not available", 0, null);
      }
    } catch (e) {
      if (isLoading) {
        stopLoading();
      }
      showAlertErr(context: context, title: url, msg: e.toString());
    }
  }

  dispose() {
    try {
      stopLoading();
      //_dio.close();
      //_dio = null;
    } catch (e) {}
  }

  Future<bool> hasNetwork() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
}
