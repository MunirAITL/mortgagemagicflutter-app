import 'dart:io';
import 'package:Mortgage_Magic/view/dashboard/DashboardScreen.dart';
import 'package:Mortgage_Magic/view/welcome/WelcomeScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mypkg/Mixin.dart';
import 'FirebaseClass.dart';
import 'config/AppDefine.dart';
import 'view/splash/SplashScreen.dart';
import 'config/MyTheme.dart';
import 'package:mypkg/controller/PinCert.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'controller/network/CookieMgr.dart';
import 'config/Server.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

//  https://github.com/fluttercommunity/flutter_after_layout
/*
If you want the whole table to be Centered, use the mainAxisAlignment property of Column.

Column
mainAxisAlignment: MainAxisAlignment.center //Center Column contents vertically,
crossAxisAlignment: CrossAxisAlignment.center //Center Column contents horizontally,

Row
mainAxisAlignment: MainAxisAlignment.center //Center Row contents horizontally,
crossAxisAlignment: CrossAxisAlignment.center //Center Row contents vertically,
*/
bool USE_FIRESTORE_EMULATOR = false;

void main() async {
  //  set permission for webRTC
  WidgetsFlutterBinding.ensureInitialized();
  await PermissionHandler()
      .requestPermissions([PermissionGroup.camera, PermissionGroup.microphone]);

  //configLoading();

  //  socket.io
  HttpOverrides.global = new MyHttpOverrides();
  //  firebase
  await Firebase.initializeApp();
  //  device settings
  SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  await SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      //DeviceOrientation.landscapeLeft,
      //DeviceOrientation.landscapeRight
    ],
  );
  if (USE_FIRESTORE_EMULATOR) {
    FirebaseFirestore.instance.settings = Settings(
        host: 'localhost:8080', sslEnabled: false, persistenceEnabled: false);
  }

  //  error catching
  FlutterError.onError = (FlutterErrorDetails details) async {
    //if (!Server.ISLIVE) {
    print("mail::FlutterErrorDetails" + details.toString());
    //}
    /*final map = Map<String, dynamic>();
    map['key'] = Define.EMAIL_KEY;
    map['subject'] = Server.APP_NAME + "::FlutterErrorDetails";
    map['msg'] =
        "<html><body><center><h1>" + details.toString() + "</h1></body></html>";
    Map<String, String> headers = {"Accept": "application/json"};
    await http.post(Define.EMAIL_URL, headers: headers, body: map);*/
  };

  runApp(new MyApp());
  //});
}

//  Progress Indicator Config Start Here...
void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.light
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = Colors.yellow
    ..backgroundColor = Colors.green
    ..indicatorColor = Colors.yellow
    ..textColor = Colors.yellow
    ..maskColor = Colors.blue.withOpacity(0.5)
    ..userInteractions = true
    ..dismissOnTap = false;
  //..customAnimation = CustomAnimation();
}

//  Progress Indicator Config End Here...

class MyApp extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: AppDefine.APP_NAME,
      theme: MyTheme.themeData,
      builder: EasyLoading.init(),
      home: new Splash(),
    );
  }
}

class Splash extends StatefulWidget {
  @override
  State createState() => _SplashState();
}

class _SplashState extends State<Splash> with Mixin {
  @override
  void initState() {
    super.initState();
    //wsSrv();
    Future.delayed(Duration.zero, () async {
      // ?s over, navigate to a new page
      try {
        //  apns
        //  https://console.firebase.google.com/project/_/notification
        //PushNotificationsManager().init();

        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0) {
          await Navigator.pushReplacement(context,
              new MaterialPageRoute(builder: (__) => DashBoardScreen()));
        } else {
          await Navigator.pushReplacement(
              context, new MaterialPageRoute(builder: (__) => WelcomeScreen()));
        }
      } catch (e) {
        log(e.toString());
      }
    });
  }

  @mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.themeData.accentColor,
        body: SplashScreen(),
      ),
    );
  }
}
