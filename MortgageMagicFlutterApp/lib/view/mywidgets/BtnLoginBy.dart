import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

import 'Btn.dart';
import 'BtnIcon.dart';

class BtnLoginBy extends StatelessWidget with Mixin {
  final Function callbackLoginMobile;
  final Function callbackFBLogin;
  final Function callbackGLogin;

  const BtnLoginBy(
      {Key key,
      @required this.callbackLoginMobile,
      @required this.callbackFBLogin,
      @required this.callbackGLogin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        Txt(
          txt: "or",
          txtColor: Colors.black,
          txtSize: 2.5,
          txtAlign: TextAlign.center,
          isBold: true,
        ),
        SizedBox(height: 10),
        (callbackLoginMobile != null)
            ? Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Btn(
                  txt: "Log in with Mobile",
                  txtColor: Colors.white,
                  bgColor: MyTheme.themeData.accentColor,
                  width: getW(context),
                  height: getHP(context, 8),
                  isCurve: false,
                  callback: () => callbackLoginMobile(),
                ),
              )
            : SizedBox(),
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: BtnIcon(
            txt: "Continue with Facebook",
            txtColor: Colors.white,
            bgColor: Colors.blue,
            width: getW(context),
            height: getHP(context, 8),
            icon: null,
            image: Image(
              image: AssetImage(
                "assets/images/icons/fb_icon.png",
              ),
              color: null,
              width: 30,
              height: 30,
            ),
            isRightIco: false,
            isCurve: false,
            callback: () => callbackFBLogin(),
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Card(
            elevation: 5,
            margin: const EdgeInsets.only(bottom: 5),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50.0),
                topRight: Radius.circular(50.0),
              ),
            ),
            child: BtnIcon(
              txt: "Sign in",
              txtColor: Colors.black,
              bgColor: Colors.white,
              width: getW(context),
              height: getHP(context, 8),
              icon: null,
              image: Image(
                image: AssetImage(
                  "assets/images/icons/g_icon.png",
                ),
                color: null,
                width: 30,
                height: 30,
              ),
              isRightIco: false,
              isCurve: false,
              callback: () => callbackGLogin(),
            ),
          ),
        ),
      ]),
    );
  }
}
