import 'package:flutter/material.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:mypkg/Mixin.dart';

class BtnOutline extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Function callback;

  const BtnOutline({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: OutlineButton(
        child: new Text(
          txt,
          style: TextStyle(
            color: txtColor,
            fontSize: 20,
          ),
        ),
        onPressed: () {
          callback();
        },
        borderSide: BorderSide(color: borderColor),
        shape: StadiumBorder(),
      ),
    );
  }
}
