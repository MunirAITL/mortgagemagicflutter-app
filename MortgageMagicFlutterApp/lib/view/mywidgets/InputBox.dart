import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class InputBox extends StatelessWidget with Mixin {
  final ctrl, lableTxt, kbType, len, isPwd;
  InputBox({
    Key key,
    @required this.ctrl,
    @required this.lableTxt,
    @required this.kbType,
    @required this.len,
    @required this.isPwd,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Colors.black,
      //height: getH(context) * .15,
      margin: const EdgeInsets.only(left: 20.0, right: 20),
      //padding: const EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(15))),
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: TextField(
          controller: ctrl,
          keyboardType: kbType,
          obscureText: isPwd,
          maxLength: len,
          autocorrect: false,
          style: TextStyle(
            color: Colors.black,
            fontSize: ResponsiveFlutter.of(context).fontSize(2),
          ),
          decoration: new InputDecoration(
            labelText: lableTxt,
            labelStyle: new TextStyle(
              color: Colors.black,
              fontSize: ResponsiveFlutter.of(context).fontSize(2),
            ),
            contentPadding: const EdgeInsets.symmetric(vertical: 0),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 3),
            ),
          ),
        ),
      ),
    );
  }
}
