import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class LoadingTxt extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Center(
        child: ScaleAnimatedTextKit(
          text: [
            "Loading...",
            "Please wait...",
          ],
          textStyle: TextStyle(fontSize: 20, color: Colors.black),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
