import 'package:flutter/material.dart';

class BoldTxt extends StatelessWidget {
  final String text1;
  final String text2;

  const BoldTxt({
    Key key,
    @required this.text1,
    @required this.text2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RichText(
        text: new TextSpan(
          // Note: Styles for TextSpans must be explicitly defined.
          // Child text spans will inherit styles from parent
          style: new TextStyle(
            fontSize: 17.0,
            color: Colors.black,
          ),
          children: <TextSpan>[
            new TextSpan(text: text1),
            new TextSpan(
                text: text2, style: new TextStyle(fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }
}
