import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class SwitchView extends StatefulWidget {
  final bool value;
  final onTxt, offTxt;
  final ValueChanged<bool> onChanged;
  final Color activeColor;

  const SwitchView({
    Key key,
    @required this.value,
    @required this.onChanged,
    @required this.activeColor,
    @required this.onTxt,
    @required this.offTxt,
  }) : super(key: key);

  @override
  State createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<SwitchView>
    with SingleTickerProviderStateMixin, Mixin {
  Animation _circleAnimation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
            begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
            end: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
            parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: Container(
            width: getWP(context, 25),
            height: getHP(context, 8),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40.0),
                color: _circleAnimation.value == Alignment.centerLeft
                    ? Colors.grey
                    : widget.activeColor),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 4.0, bottom: 4.0, right: 4.0, left: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _circleAnimation.value == Alignment.centerRight
                      ? Padding(
                          padding:
                              const EdgeInsets.only(left: 10.0, right: 5.0),
                          child: Txt(
                            txt: widget.onTxt,
                            txtColor: Colors.white,
                            txtSize: 2,
                            txtAlign: TextAlign.center,
                            isBold: true,
                          ),
                        )
                      : Container(),
                  Align(
                    alignment: _circleAnimation.value,
                    child: Container(
                      width: 25.0,
                      height: 25.0,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.white),
                    ),
                  ),
                  _circleAnimation.value == Alignment.centerLeft
                      ? Padding(
                          padding:
                              const EdgeInsets.only(left: 5.0, right: 10.0),
                          child: Txt(
                            txt: widget.offTxt,
                            txtColor: Colors.white,
                            txtSize: 2,
                            txtAlign: TextAlign.center,
                            isBold: true,
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
