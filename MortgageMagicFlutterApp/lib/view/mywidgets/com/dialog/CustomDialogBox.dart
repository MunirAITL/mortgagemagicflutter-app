import 'dart:ui';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class CustomDialogBox extends StatefulWidget {
  @override
  State createState() => _CustomDialogBoxState();
}

class _CustomDialogBoxState extends State<CustomDialogBox> with Mixin {
  List<String> listTips = [
    "To request a Mortgage Case, tap Create Case Icon",
    "To see all your Cases, tap on the My cases icon",
    "To view your Messages, tap on the Messages icon",
    "To view your Notifications, tap on the Notifications icon",
    "Tap on More icon to manage your profile and other settings"
  ];

  List<String> listTab = [
    "Create Case",
    "My Cases",
    "Messages",
    "Notifications",
    "More"
  ];

  int index = 0;

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    listTips = null;
    _stateProvider = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Container(
      height: getHP(context, 30),
      color: MyTheme.themeData.accentColor,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
              txt: listTips[index],
              txtColor: Colors.white,
              txtSize: 2.5,
              txtAlign: TextAlign.center,
              isBold: true,
              txtLineSpace: 1.5,
            ),
          ),
          //Spacer(),
          Positioned(
            child: new Align(
              alignment: FractionalOffset.topRight,
              child: IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    //
                    Navigator.of(context).pop();
                  }),
            ),
          ),
          Positioned(
            child: new Align(
              alignment: FractionalOffset.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Container(
                  color: Colors.black,
                  //height: getHP(context, 6),
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      (index == 0)
                          ? SizedBox()
                          : IconButton(
                              iconSize: 50,
                              icon: Icon(
                                Icons.arrow_left,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                //
                                setState(() {
                                  index--;
                                  _switchTabsHand(index);
                                });
                              },
                            ),
                      Expanded(
                        child: Txt(
                          txt: listTab[index].toString(),
                          txtColor: Colors.white,
                          txtSize: 2.5,
                          txtAlign: TextAlign.center,
                          isBold: true,
                        ),
                      ),
                      (index == listTips.length - 1)
                          ? SizedBox()
                          : IconButton(
                              iconSize: 50,
                              icon: Icon(
                                Icons.arrow_right,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                //
                                setState(() {
                                  index++;
                                  _switchTabsHand(index);
                                });
                              },
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _switchTabsHand(int index2) {
    switch (index2) {
      case 0:
        _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar1);
        break;
      case 1:
        _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar2);
        break;
      case 2:
        _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar3);
        break;
      case 3:
        _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar4);
        break;
      case 4:
        _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar5);
        break;
      default:
    }
  }
}
