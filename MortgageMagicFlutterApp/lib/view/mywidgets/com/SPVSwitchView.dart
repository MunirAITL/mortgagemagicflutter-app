import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/view/mywidgets/InputBox.dart';
import 'package:Mortgage_Magic/view/mywidgets/SwitchView.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:intl/intl.dart';
import '../IcoTxtIco.dart';

class SPVSwitchView extends StatefulWidget {
  final Function(String) callback;
  final TextEditingController compName;
  final TextEditingController regAddr;
  final TextEditingController regNo;
  SPVSwitchView({
    Key key,
    @required this.compName,
    @required this.regAddr,
    @required this.regNo,
    @required this.callback,
  }) : super(key: key);

  @override
  State createState() => _SPVSwitchViewState();
}

class _SPVSwitchViewState extends State<SPVSwitchView> with Mixin {
  bool isSwitch = false;
  String dt = "";
  String dd = "";
  String mm = "";
  String yy = "";
  //final Function(String) callback;

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //_stateProvider.dispose();
    _stateProvider = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Txt(
                    txt:
                        "Are you buying the property in the name of a SPV (Limited Company)?",
                    txtColor: Colors.black,
                    txtSize: 2,
                    txtAlign: TextAlign.start,
                    isBold: false,
                    txtLineSpace: 1.5,
                  ),
                ),
                SizedBox(width: 10),
                SwitchView(
                  activeColor: MyTheme.themeData.accentColor,
                  value: isSwitch,
                  onTxt: 'Yes',
                  offTxt: 'No',
                  onChanged: (value) {
                    isSwitch = value;
                    if (mounted) {
                      setState(() {
                        _stateProvider
                            .notify(ObserverState.STATE_CHANGED_spvswitchview);
                      });
                    }
                    //callback((isSwitch) ? _companyName.text.trim() : '');
                  },
                ),
              ],
            ),
          ),
          (isSwitch) ? drawSPVInputFields() : SizedBox(),
        ],
      ),
    );
  }

  drawSPVInputFields() {
    return Container(
      child: Column(
        children: [
          //SizedBox(height: 10),
          InputBox(
            ctrl: widget.compName,
            lableTxt: "Name of Company",
            kbType: TextInputType.text,
            len: 100,
            isPwd: false,
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: widget.regAddr,
            lableTxt: "Registered Address",
            kbType: TextInputType.text,
            len: 200,
            isPwd: false,
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: widget.regAddr,
            lableTxt: "Company Registeration Number",
            kbType: TextInputType.text,
            len: 200,
            isPwd: false,
          ),
          SizedBox(height: 20),
          drawRegDate(),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  drawRegDate() {
    DateTime date = DateTime.now();
    var newDate = new DateTime(date.year, date.month, date.day);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Date registered",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
          ),
          SizedBox(height: 5),
          GestureDetector(
            onTap: () {
              /*
                initialDate : Default Selected Date In Picker Dialog
                firstDate : From Minimum Date Of Your Date Picker
                lastDate : Max Date Of To-Date Of Date Picker
              */
              showDatePicker(
                context: context,
                initialDate: newDate,
                firstDate: DateTime(1950),
                lastDate: newDate,
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light(), // This will change to light theme.
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  if (mounted) {
                    setState(() {
                      try {
                        dt = DateFormat('dd-MMM-yyyy').format(value).toString();
                        final dobArr = date.toString().split('-');
                        this.dd = dobArr[0];
                        this.mm = dobArr[1];
                        this.yy = dobArr[2];
                        widget.callback(dt);
                      } catch (e) {
                        log(e.toString());
                      }
                    });
                  }
                }
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: IcoTxtIco(
                leftIcon: Icons.calendar_today,
                txt: (dt == "") ? "Select a date" : dt,
                rightIcon: Icons.arrow_drop_down,
                txtAlign: TextAlign.left,
                rightIconSize: 50,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
