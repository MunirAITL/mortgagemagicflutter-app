import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/view/mywidgets/InputBox.dart';
import 'package:Mortgage_Magic/view/mywidgets/SwitchView.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class CompSwitchView extends StatefulWidget {
  //final Function(String) callback;
  final TextEditingController compName;
  CompSwitchView({
    Key key,
    @required this.compName,
  }) : super(key: key);

  @override
  State createState() => _CompSwitchViewState();
}

class _CompSwitchViewState extends State<CompSwitchView> with Mixin {
  bool isSwitch = true;
  //final Function(String) callback;

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Txt(
                    txt:
                        "Do you have a mortgage company? Otherwise we will recommend to you.",
                    txtColor: Colors.black,
                    txtSize: 2,
                    txtAlign: TextAlign.start,
                    isBold: false,
                    txtLineSpace: 1.5,
                  ),
                ),
                SizedBox(width: 10),
                SwitchView(
                  activeColor: MyTheme.themeData.accentColor,
                  value: isSwitch,
                  onTxt: 'Yes',
                  offTxt: 'No',
                  onChanged: (value) {
                    isSwitch = value;
                    if (mounted) {
                      _stateProvider.notify(ObserverState.STATE_CHANGED);
                      setState(() {});
                    }
                    //callback((isSwitch) ? _companyName.text.trim() : '');
                  },
                ),
              ],
            ),
          ),
          (isSwitch) ? drawComName() : SizedBox(),
        ],
      ),
    );
  }

  drawComName() {
    return Container(
      child: Column(
        children: [
          //SizedBox(height: 10),
          InputBox(
            ctrl: widget.compName,
            lableTxt: "Company Name",
            kbType: TextInputType.text,
            len: 100,
            isPwd: false,
          ),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
