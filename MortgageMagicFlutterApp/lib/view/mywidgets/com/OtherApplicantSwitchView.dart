import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/view/mywidgets/InputBox.dart';
import 'package:Mortgage_Magic/view/mywidgets/SwitchView.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class OtherApplicantSwitchView extends StatefulWidget {
  final Function(int) callback;
  List<TextEditingController> listOApplicantInputFieldsCtr;
  OtherApplicantSwitchView({
    Key key,
    @required this.listOApplicantInputFieldsCtr,
    @required this.callback,
  }) : super(key: key);

  @override
  State createState() => _OtherApplicantSwitchViewState();
}

enum RadioEnum { one, two, three }

class _OtherApplicantSwitchViewState extends State<OtherApplicantSwitchView>
    with Mixin {
  bool isSwitch = false;

  StateProvider _stateProvider = StateProvider();

  RadioEnum _character = RadioEnum.one;

  List<String> listInputFieldsTitle = [
    "Please enter Second Applicant's email address:",
    "Please enter Third Applicant's email address:",
    "Please enter Fourth Applicant's email address:"
  ];
  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Txt(
                    txt: "Is there other applicant?",
                    txtColor: Colors.black,
                    txtSize: 2,
                    txtAlign: TextAlign.start,
                    isBold: false,
                    txtLineSpace: 1.5,
                  ),
                ),
                SizedBox(width: 10),
                SwitchView(
                  activeColor: MyTheme.themeData.accentColor,
                  value: isSwitch,
                  onTxt: 'Yes',
                  offTxt: 'No',
                  onChanged: (value) {
                    isSwitch = value;
                    if (mounted) {
                      setState(() {
                        _stateProvider.notify(ObserverState.STATE_CHANGED);
                      });
                    }
                    //callback((isSwitch) ? _companyName.text.trim() : '');
                  },
                ),
              ],
            ),
          ),
          (isSwitch) ? drawApplicantView() : SizedBox(),
        ],
      ),
    );
  }

  drawApplicantView() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt:
                  "How many people do you want named on the case application?*",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.start,
              isBold: false,
              txtLineSpace: 1.2,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt:
                  "It's ok if the person you're applying with doesn't have an income-they can still be named on your case application. Choose an option from the below One, Two or Three",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.start,
              isBold: false,
              txtLineSpace: 1.2,
            ),
          ),
          drawApplicantRadioView(),
          drawInputFieldsView(),
        ],
      ),
    );
  }

  drawApplicantRadioView() {
    return Container(
      //color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 5),
          Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            color: Colors.white,
            child: Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: Colors.black,
                disabledColor: Colors.black,
                selectedRowColor: Colors.black,
                indicatorColor: Colors.black,
                toggleableActiveColor: Colors.black,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _character = RadioEnum.one;
                          widget.callback(1);
                        });
                      }
                    },
                    child: Row(children: <Widget>[
                      Radio(
                        value: RadioEnum.one,
                        groupValue: _character,
                        onChanged: (RadioEnum value) {
                          if (mounted) {
                            setState(() {
                              _character = value;
                              widget.callback(1);
                            });
                          }
                        },
                      ),
                      Txt(
                        txt: "One",
                        txtColor: Colors.black,
                        txtSize: 2,
                        txtAlign: TextAlign.center,
                        isBold: false,
                      ),
                    ]),
                  ),
                  SizedBox(width: 20),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _character = RadioEnum.two;
                          widget.callback(2);
                        });
                      }
                    },
                    child: Row(children: <Widget>[
                      Radio(
                        value: RadioEnum.two,
                        groupValue: _character,
                        onChanged: (RadioEnum value) {
                          if (mounted) {
                            setState(() {
                              _character = value;
                              widget.callback(2);
                            });
                          }
                        },
                      ),
                      Txt(
                        txt: "Two",
                        txtColor: Colors.black,
                        txtSize: 2,
                        txtAlign: TextAlign.center,
                        isBold: false,
                      ),
                    ]),
                  ),
                  SizedBox(width: 20),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            _character = RadioEnum.three;
                            widget.callback(3);
                          });
                        }
                      },
                      child: Row(children: <Widget>[
                        Radio(
                          value: RadioEnum.three,
                          groupValue: _character,
                          onChanged: (RadioEnum value) {
                            if (mounted) {
                              setState(() {
                                _character = value;
                                widget.callback(3);
                              });
                            }
                          },
                        ),
                        Txt(
                          txt: "Three",
                          txtColor: Colors.black,
                          txtSize: 2,
                          txtAlign: TextAlign.center,
                          isBold: false,
                        ),
                      ]),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawInputFieldsView() {
    try {
      int totalInputFields = 0;
      switch (_character.index) {
        case 0:
          totalInputFields = 1;
          break;
        case 1:
          totalInputFields = 2;
          break;
        case 2:
          totalInputFields = 3;
          break;
        default:
      }
      return (totalInputFields == 0)
          ? SizedBox()
          : Container(
              child: Column(
                children: [
                  //SizedBox(height: 10),
                  for (int i = 0; i < totalInputFields; i++)
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 25, right: 25),
                            child: Txt(
                                txt: listInputFieldsTitle[i],
                                txtColor: Colors.black,
                                txtSize: 2,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                          InputBox(
                            ctrl: widget.listOApplicantInputFieldsCtr[i],
                            lableTxt: "applicant" +
                                (i + 2).toString() +
                                "@example.com",
                            kbType: TextInputType.emailAddress,
                            len: 100,
                            isPwd: false,
                          ),
                        ],
                      ),
                    ),
                ],
              ),
            );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
