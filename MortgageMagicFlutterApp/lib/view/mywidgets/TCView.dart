import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class TCView extends StatelessWidget with Mixin {
  final screenName;
  Color txt1Color;
  Color txt2Color;

  TCView({
    @required this.screenName,
    this.txt1Color = Colors.black,
    this.txt2Color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        child: Center(
          child: RichText(
            text: TextSpan(
                text: "By clicking on '" +
                    screenName +
                    "' you confirm that you accept the ",
                style: TextStyle(
                    color: txt1Color,
                    fontSize: ResponsiveFlutter.of(context).fontSize(2.2)),
                children: <TextSpan>[
                  TextSpan(
                      text: 'Mortgage Magic Terms and Conditions',
                      style: TextStyle(
                          color: (txt2Color == null)
                              ? MyTheme.themeData.accentColor
                              : txt2Color,
                          fontSize: ResponsiveFlutter.of(context).fontSize(2.2),
                          fontWeight: FontWeight.bold),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          // navigate to desired screen
                          openUrl(context, Server.TC_URL);
                        })
                ]),
          ),
        ));
  }
}
