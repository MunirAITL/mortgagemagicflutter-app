import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class IcoTxtIco extends StatelessWidget with Mixin {
  final IconData leftIcon;
  final IconData rightIcon;
  final txt;
  TextAlign txtAlign;
  double leftIconSize;
  double rightIconSize;
  double height;
  Color iconColor;

  IcoTxtIco(
      {Key key,
      @required this.txt,
      @required this.leftIcon,
      @required this.rightIcon,
      this.iconColor = Colors.black,
      this.txtAlign = TextAlign.center,
      this.leftIconSize = 30,
      this.rightIconSize = 30,
      this.height = 10})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHP(context, height),
      //color: Colors.white,
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey,
          ),
          borderRadius: BorderRadius.all(Radius.circular(20))),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            (leftIcon != null)
                ? Icon(
                    leftIcon,
                    color: iconColor,
                    size: leftIconSize,
                  )
                : SizedBox(),
            SizedBox(width: 10),
            Expanded(
              child: Txt(
                txt: txt,
                txtColor: Colors.black,
                txtSize: 2,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
            ),
            (rightIcon != null)
                ? Icon(
                    rightIcon,
                    color: iconColor,
                    size: rightIconSize,
                  )
                : SizedBox(),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
