import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class Btn extends StatelessWidget {
  final String txt;
  final Color txtColor;
  final Color bgColor;
  final double width;
  final double height;
  final bool isCurve;
  final Function callback;

  Btn({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.bgColor,
    @required this.width,
    @required this.height,
    @required this.isCurve,
    @required this.callback,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      //color: Colors.brown,
      child: RaisedButton(
        elevation: 10,
        child: Txt(
          txt: txt,
          txtColor: txtColor,
          txtSize: 2,
          txtAlign: TextAlign.center,
          isBold: true,
        ),
        onPressed: () => callback(),
        //textColor: Colors.black,
        color: bgColor,
        shape: OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid,
                width: 1.0,
                color: Colors.transparent),
            borderRadius: new BorderRadius.circular((isCurve) ? 20 : 0)),
      ),
    );
  }
}
