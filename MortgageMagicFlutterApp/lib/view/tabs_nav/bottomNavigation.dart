import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/dashboard/DashboardScreen.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'tabItem.dart';
import 'package:badges/badges.dart';

class BottomNavigation extends StatelessWidget with Mixin {
  final BuildContext context;
  final ValueChanged<int> onSelectTab;
  final List<TabItem> tabs;
  final bool isHelpTut;
  final int totalMsg;
  final int totalNoti;
  BottomNavigation({
    @required this.context,
    @required this.onSelectTab,
    @required this.tabs,
    @required this.isHelpTut,
    @required this.totalMsg,
    @required this.totalNoti,
  });

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      items: tabs
          .map(
            (e) => _buildItem(
              index: e.getIndex(),
              icon: e.icon,
              tabName: e.tabName,
            ),
          )
          .toList(),
      onTap: (index) => onSelectTab(
        index,
      ),
    );
  }

  BottomNavigationBarItem _buildItem(
      {int index, AssetImage icon, String tabName}) {
    final topBadgePos = getHP(context, 4);
    final endBadgePos = getHP(context, 2);
    final int totalBadge = (index == 2 && totalMsg > 0)
        ? totalMsg
        : (index == 3 && totalNoti > 0)
            ? totalNoti
            : 0;
    return BottomNavigationBarItem(
      icon: (isHelpTut && DashBoardScreenState.currentTab == index)
          ? Stack(overflow: Overflow.visible, children: <Widget>[
              (index == 2 || index == 3)
                  ? Badge(
                      showBadge: (totalBadge > 0) ? true : false,
                      position: BadgePosition.topEnd(
                          top: -topBadgePos, end: -endBadgePos),
                      badgeContent: Text(
                        totalBadge.toString(),
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      child: ImageIcon(
                        icon,
                        color: _tabColor(index: index),
                      ),
                    )
                  : ImageIcon(
                      icon,
                      color: _tabColor(index: index),
                    ),
              new Positioned(
                top: -getHP(context, 7),
                right: 0.0,
                child: Image.asset(
                  "assets/images/icons/help_hand.png",
                  width: 50,
                  height: 50,
                ),
              )
            ])
          : Badge(
              showBadge: (totalBadge > 0) ? true : false,
              position:
                  BadgePosition.topEnd(top: -topBadgePos, end: -endBadgePos),
              badgeContent: Text(
                totalBadge.toString(),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              child: ImageIcon(
                icon,
                color: _tabColor(index: index),
              ),
            ),
      title: Text(
        tabName,
        style: TextStyle(
            color: _tabColor(index: index),
            fontSize: 14,
            fontWeight: (DashBoardScreenState.currentTab == index)
                ? FontWeight.bold
                : FontWeight.normal),
      ),
    );
  }

  Color _tabColor({int index}) {
    return DashBoardScreenState.currentTab == index
        ? MyTheme.themeData.accentColor
        : Colors.black;
  }
}
