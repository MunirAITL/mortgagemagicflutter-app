import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/auth/AuthScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/BtnIcon.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          //resizeToAvoidBottomPadding: false,
          body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onPanDown: (detail) {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          //decoration: MyTheme.boxDeco,
          height: getH(context),
          color: MyTheme.themeData.accentColor,
          child: SingleChildScrollView(
            child: Column(
              children: [
                drawTopLogo(),
                drawCenterImage(),
                drawMMText(),
                SizedBox(height: getHP(context, 5)),
                Container(
                  width: getWP(context, 90),
                  child: BtnIcon(
                    txt: "Let's Start",
                    txtColor: Colors.black,
                    bgColor: Colors.white,
                    width: getWP(context, 80),
                    height: getHP(context, 10),
                    icon: Icons.arrow_right,
                    image: null,
                    isRightIco: true,
                    isCurve: true,
                    callback: () async {
                      await Navigator.pushReplacement(context,
                          new MaterialPageRoute(builder: (__) => AuthScreen()));
                    },
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      )),
    );
  }

  drawTopLogo() {
    return Container(
      width: getW(context),
      height: getHP(context, 35),
      child: Column(
        children: [
          SizedBox(height: getHP(context, 10)),
          Container(
              width: getWP(context, 70),
              child: Image.asset(
                'assets/images/logo/mm.png',
                fit: BoxFit.fitWidth,
              )),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20, top: 20),
            child: Txt(
              txt: "Welcome to the Mortgage Magic App",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.center,
              isBold: true,
            ),
          ),
          //SizedBox(height: 20),
        ],
      ),
    );
  }

  drawCenterImage() {
    return Container(
      width: getW(context),
      height: getHP(context, 35),
      child: Image.asset(
        'assets/images/screens/welcome/welcome_theme.png',
        fit: BoxFit.fill,
      ),
    );
  }

  drawMMText() {
    return Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Txt(
            txt:
                "By using the Mortgage Magic app you can get different types of solutions for Residential Mortgage/loans/Development Finance etc.",
            txtColor: MyTheme.themeData.accentColor,
            txtSize: 2.5,
            txtAlign: TextAlign.center,
            isBold: true,
            txtLineSpace: 1.2,
          ),
        ));
  }
}
