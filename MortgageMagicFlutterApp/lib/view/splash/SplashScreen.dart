import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class SplashScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Center(
            child: Column(
              children: <Widget>[
                Container(
                    width: getWP(context, 65),
                    child: Image.asset(
                      'assets/images/logo/splash_logo.png',
                      fit: BoxFit.fitWidth,
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
