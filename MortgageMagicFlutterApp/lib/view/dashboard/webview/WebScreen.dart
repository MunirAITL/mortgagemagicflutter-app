import 'dart:io';
import 'package:Mortgage_Magic/config/AppDefine.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/controller/network/CookieMgr.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class WebScreen extends StatefulWidget {
  final String url;
  final String title;

  const WebScreen({
    Key key,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _WebScreenState();
}

class _WebScreenState extends State<WebScreen> with Mixin {
  InAppWebViewController webView;
  double progress = 0;
  String cookieStr;

  final CookieManager cookieManager = CookieManager.instance();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    cookieStr = null;
    webView = null;
    super.dispose();
  }

  appInit() async {
    try {
      CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      log(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Server.BASE_URL,
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires.,
          isSecure: true,
        );
        setState(() {});
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: widget.title,
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          child: Column(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: drawWebView(),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: drawBottomNavBar(),
                    ),
                    Align(
                        alignment: Alignment.topCenter,
                        child: _buildProgressBar()),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawWebView() {
    return InAppWebView(
      initialUrl: widget.url,
      //initialHeaders: {'Cookie': cookieStr},
      initialOptions: InAppWebViewGroupOptions(
          crossPlatform: InAppWebViewOptions(
        debuggingEnabled: true,
        preferredContentMode: UserPreferredContentMode.DESKTOP,
      )),
      onWebViewCreated: (InAppWebViewController controller) {
        webView = controller;
      },
      onLoadStart: (InAppWebViewController controller, String url) {},
      onLoadStop: (InAppWebViewController controller, String url) async {},
      onProgressChanged: (InAppWebViewController controller, int progress) {
        setState(() {
          this.progress = progress / 100;
        });
      },
    );
  }

  Widget _buildProgressBar() {
    if (progress != 1.0) {
      //return CircularProgressIndicator();
      // You can use LinearProgressIndicator also
      return LinearProgressIndicator(
        value: progress,
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.green),
        backgroundColor: Colors.grey,
      );
    }
    return Container();
  }

  drawBottomNavBar() {
    return ButtonBar(
      alignment: MainAxisAlignment.center,
      children: <Widget>[
        RaisedButton(
          color: Colors.white,
          child: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            if (webView != null) {
              webView.goBack();
            }
          },
        ),
        RaisedButton(
          color: Colors.white,
          child: Icon(Icons.arrow_forward, color: Colors.black),
          onPressed: () {
            if (webView != null) {
              webView.goForward();
            }
          },
        ),
        RaisedButton(
          color: Colors.white,
          child: Icon(
            Icons.refresh,
            color: Colors.black,
          ),
          onPressed: () {
            if (webView != null) {
              webView.reload();
            }
          },
        ),
      ],
    );
  }
}
