import 'dart:async';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'dart:io';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class TestWeb extends StatefulWidget {
  final String url;
  final String title;

  const TestWeb({
    Key key,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _TestWebState();
}

class _TestWebState extends State<TestWeb> with Mixin {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
  /*final flutterWebviewPlugin = new FlutterWebviewPlugin();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    flutterWebviewPlugin.close();
    super.dispose();
  }

  appInit() {
    try {
      // Enable hybrid composition.
      flutterWebviewPlugin.onUrlChanged.listen((String url) {
        log("onUrlChanged::" + url);
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WebviewScaffold(
        url: widget.url,
        withLocalStorage: true,
        //withZoom: true,
        hidden: true,
        //resizeToAvoidBottomInset: true,
        //appCacheEnabled: true,
        debuggingEnabled: (Server.isTest) ? true : false,
        clearCookies: false,
        mediaPlaybackRequiresUserGesture: true,
        ignoreSSLErrors: true,
        javascriptChannels: jsChannels,
        withJavascript: true,
        initialChild: Container(
          color: Colors.white,
          child: Center(
            child: Txt(
                txt: "Loading...",
                txtColor: Colors.black,
                txtSize: 2,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
        ),
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "New Case -" + widget.title,
              txtColor: Colors.white,
              txtSize: 2.5,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ),
      ),
    );
  }

  // ignore: prefer_collection_literals
  final Set<JavascriptChannel> jsChannels = [
    JavascriptChannel(
        name: 'Print',
        onMessageReceived: (JavascriptMessage message) {
          print(message.message);
        }),
  ].toSet();*/
}
