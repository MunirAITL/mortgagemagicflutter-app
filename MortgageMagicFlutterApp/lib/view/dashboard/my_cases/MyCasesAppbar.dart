import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

Widget MyCasesAppbar(
    {BuildContext context, TabController tabController, double w, double h}) {
  return AppBar(
    elevation: 10,
    backgroundColor: MyTheme.themeData.accentColor,
    title: Txt(
        txt: "My Cases",
        txtColor: Colors.white,
        txtSize: 2.2,
        txtAlign: TextAlign.start,
        isBold: true),
    centerTitle: false,
    bottom: PreferredSize(
      preferredSize: new Size(w, h * 14 / 100),
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        //crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: w,
            height: h * 5 / 100,
            //margin: const EdgeInsets.only(bottom: 10.0),
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            color: Colors.grey,
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt: "View the ongoing cases",
                  txtColor: null,
                  txtSize: 2.5,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
          ),
          TabBar(
            controller: tabController,
            isScrollable: true,
            indicatorColor: Colors.white,
            unselectedLabelColor: Colors.white70,
            labelColor: Colors.white,
            /*indicator: UnderlineTabIndicator(
                              borderSide:
                                  BorderSide(width: 5.0, color: Colors.white),
                              insets: EdgeInsets.symmetric(horizontal: 16.0)),*/
            tabs: [
              Tab(
                  child: Txt(
                      txt: "All",
                      txtColor: null,
                      txtSize: 2,
                      txtAlign: TextAlign.center,
                      isBold: true)),
              Tab(
                  child: Txt(
                      txt: "In-Progress",
                      txtColor: null,
                      txtSize: 2,
                      txtAlign: TextAlign.center,
                      isBold: true)),
              Tab(
                  child: Txt(
                      txt: "Submitted",
                      txtColor: null,
                      txtSize: 2,
                      txtAlign: TextAlign.center,
                      isBold: true)),
              Tab(
                  child: Txt(
                      txt: "FMA Submitted",
                      txtColor: null,
                      txtSize: 2,
                      txtAlign: TextAlign.center,
                      isBold: true)),
              Tab(
                  child: Txt(
                      txt: "Completed",
                      txtColor: null,
                      txtSize: 2,
                      txtAlign: TextAlign.center,
                      isBold: true)),
            ],
          ),
        ],
      ),
    ),
  );
}
