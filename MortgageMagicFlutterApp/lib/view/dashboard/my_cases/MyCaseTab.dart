import 'package:Mortgage_Magic/config/AppConfig.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/dashboard/NewCaseCfg.dart';
import 'package:Mortgage_Magic/controller/helper/NewCaseHelper.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/controller/helper/CaseDetailsWebHelper.dart';
import 'package:Mortgage_Magic/controller/helper/MyCaseHelper.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/LocationsModel.dart';
import 'package:Mortgage_Magic/model/json/MortgageCaseInfoEntityModelListModel.dart';
import 'package:Mortgage_Magic/model/json/TaskInfoSearchAPIModel.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/dashboard/webview/WebScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

import 'MyCasesAppbar.dart';

class MyCaseTab extends StatefulWidget {
  MyCaseTab({
    Key key,
  }) : super(key: key);

  @override
  State createState() => _MyCaseTabState();
}

class _MyCaseTabState extends State<MyCaseTab>
    with Mixin, SingleTickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TabController _tabController;
  List<LocationsModel> listTaskInfoSearchModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  //  tab stuff start here
  int caseStatus = NewCaseCfg.ALL;
  int totalTabs = 5;

  wsOnPageLoad() async {
    try {
      setState(() {
        isLoading = true;
      });

      final UserModel userModel = await DBMgr.shared.getUserProfile();
      final url = MyCaseHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount,
        userModel: userModel,
        caseStatus: caseStatus.toString(),
      );
      log(url);
      await NetworkMgr()
          .getData<TaskInfoSearchAPIModel, Null>(
        context: context,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> locations = model.responseData.locations;
                if (locations != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (locations.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (LocationsModel location in locations) {
                      listTaskInfoSearchModel.add(location);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listTaskInfoSearchModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showAlert(context: context, msg: "Cases not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        } else {
          log("not in");
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTaskInfoSearchModel.clear();
    wsOnPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
      wsOnPageLoad();
    } catch (e) {}
  }

  @override
  void dispose() {
    _tabController.dispose();
    _tabController = null;
    listTaskInfoSearchModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _tabController = new TabController(vsync: this, length: totalTabs);
      _tabController.addListener(() {
        if (!isLoading) {
          controlTabbarIndex(_tabController.index);
          log('my tab index is: ' + _tabController.index.toString());
        }
      });
    } catch (e) {}
  }

  controlTabbarIndex(int index) {
    try {
      switch (index) {
        case 0:
          caseStatus = NewCaseCfg.ALL;
          break;
        case 1:
          caseStatus = NewCaseCfg.IN_PROGRESS;
          break;
        case 2:
          caseStatus = NewCaseCfg.SUBMITTED;
          break;
        case 3:
          caseStatus = NewCaseCfg.FMA_SUBMITTED;
          break;
        case 4:
          caseStatus = NewCaseCfg.COMPLETED;
          break;
        default:
      }
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: totalTabs,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          appBar: MyCasesAppbar(
            context: context,
            tabController: _tabController,
            w: getW(context),
            h: getH(context),
          ),
          body: TabBarView(
            physics: (isLoading)
                ? NeverScrollableScrollPhysics()
                : AlwaysScrollableScrollPhysics(),
            controller: _tabController,
            children: <Widget>[
              drawRecentCases(),
              drawRecentCases(),
              drawRecentCases(),
              drawRecentCases(),
              drawRecentCases(),
            ],
          ),
        ),
      ),
    );
  }

  drawRecentCases() {
    try {
      return Container(
        child: (listTaskInfoSearchModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      wsOnPageLoad();
                    }
                  }
                  return true;
                },
                child: RefreshIndicator(
                  onRefresh: _getRefreshData,
                  child: ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: listTaskInfoSearchModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      //LocationsModel model = modelTaskInfoSearch[index];
                      if (mounted) {
                        return drawRecentCaseItem(index);
                      }
                    },
                  ),
                ),
              )
            : (!isLoading)
                ? Padding(
                    padding: const EdgeInsets.all(20),
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: getWP(context, 50),
                            height: getHP(context, 30),
                            child: Image.asset(
                              'assets/images/screens/home/my_cases/case_nf.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                          SizedBox(height: 20),
                          Txt(
                            txt: "Looks like you haven't created any case?",
                            txtColor: Colors.black,
                            txtSize: 2.5,
                            txtAlign: TextAlign.center,
                            isBold: false,
                            txtLineSpace: 1.2,
                          ),
                          SizedBox(height: 20),
                          GestureDetector(
                            onTap: () {
                              wsOnPageLoad();
                            },
                            child: Txt(
                                txt: "Refresh",
                                txtColor: MyTheme.themeData.accentColor,
                                txtSize: 2,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
      );
    } catch (e) {}
  }

  drawRecentCaseItem(index) {
    try {
      LocationsModel model = listTaskInfoSearchModel[index];
      if (model == null) return SizedBox();
      final icon = NewCaseHelper().getCreateCaseIconByTitle(model.title);
      if (icon == null) return SizedBox();

      return GestureDetector(
        onTap: () async {
          try {
            final List<dynamic> mortgageCaseInfoEntityModelListModel =
                model.mortgageCaseInfoEntityModelListModel;
            int taskId = 0;
            try {
              if (mortgageCaseInfoEntityModelListModel.length > 0) {
                final MortgageCaseInfoEntityModelListModel model =
                    mortgageCaseInfoEntityModelListModel[0];
                taskId = model.taskID;
              }
            } catch (e) {
              log(e.toString());
            }
            await Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (__) => WebScreen(
                          title: model.title,
                          url: CaseDetailsWebHelper()
                              .getLink(model.title, taskId),
                        )));
          } catch (e) {}
        },
        child: Container(
          height: getHP(context, 25),
          //color: Colors.blue,
          child: Card(
            color: Colors.white,
            child: Row(
              //contentPadding:
              //EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
              //dense: true,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: getWP(context, 25),
                  //color: Colors.yellow,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: CircleAvatar(
                          backgroundImage: AssetImage(icon),
                          radius: 30,
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                      SizedBox(height: 2),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Txt(
                            txt: model.title,
                            txtColor: Colors.black,
                            txtSize: 1.8,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Txt(
                            txt: model.title,
                            txtColor: MyTheme.themeData.accentColor,
                            txtSize: 2,
                            txtAlign: TextAlign.center,
                            isBold: true),
                        SizedBox(height: 10),
                        Container(
                          color: NewCaseHelper()
                              .getCaseStatusColor(model.caseStatus),
                          width: double.infinity,
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: Txt(
                                txt: model.status,
                                txtColor: Colors.black,
                                txtSize: 2,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Container(
                  //width: getWP(context, 20),
                  height: getWP(context, 8),
                  child: Btn(
                    txt: "View",
                    txtColor: Colors.white,
                    bgColor: MyTheme.themeData.accentColor,
                    width: getWP(context, 30),
                    height: getHP(context, 8),
                    isCurve: true,
                    callback: () {},
                  ),
                ),
                SizedBox(width: 5),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
