import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/network/CookieMgr.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/main.dart';
import 'package:Mortgage_Magic/view/dashboard/more/MoreTab.dart';
import 'package:Mortgage_Magic/view/dashboard/msg/TimeLineTab.dart';
import 'package:Mortgage_Magic/view/dashboard/my_cases/MyCaseTab.dart';
import 'package:Mortgage_Magic/view/dashboard/new_case/NewCaseTab.dart';
import 'package:Mortgage_Magic/view/dashboard/noti/NotiTab.dart';
import 'package:Mortgage_Magic/view/mywidgets/com/dialog/CustomDialogBox.dart';
import 'package:Mortgage_Magic/view/tabs_nav/bottomNavigation.dart';
import 'package:Mortgage_Magic/view/tabs_nav/tabItem.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class DashBoardScreen extends StatefulWidget {
  @override
  State createState() => DashBoardScreenState();
}

class DashBoardScreenState extends State<DashBoardScreen>
    with Mixin, StateListener, TickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider;
  var dialogHelp;

  static int currentTab = 0;

  @override
  onStateChanged(ObserverState state) async {
    int tabIndex = 0;
    if (state == ObserverState.STATE_CHANGED_logout) {
      CookieMgr().delCookiee();
      Navigator.pop(context);
      await Navigator.push(
          context, new MaterialPageRoute(builder: (__) => MyApp()));
    } else if (state == ObserverState.STATE_CHANGED_tabbar1) {
      tabIndex = 0;
      if (dialogHelp == null) {
        dialogHelp = showDialog(
            context: context,
            barrierDismissible: true,
            barrierColor: Colors.black.withOpacity(0.5),
            builder: (BuildContext context) {
              return CustomDialogBox();
            }).then((value) {
          dialogHelp = null;
          setState(() {
            _selectTab(0);
          });
        });
      }
      setState(() {
        _selectTab(0);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar2) {
      tabIndex = 1;
      setState(() {
        currentTab = tabIndex;
        _selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar3) {
      tabIndex = 2;
      setState(() {
        currentTab = tabIndex;
        _selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar4) {
      tabIndex = 3;
      setState(() {
        currentTab = tabIndex;
        _selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar5) {
      tabIndex = 4;
      setState(() {
        currentTab = tabIndex;
        _selectTab(currentTab);
      });
    }
  }

  //  Tabbar stuff start here...
  final List<TabItem> tabs = [
    TabItem(
      tabName: "New Case",
      icon: AssetImage("assets/images/tabbar/new_case_icon.png"),
      page: NewCaseTab(),
    ),
    TabItem(
      tabName: "My Cases",
      icon: AssetImage("assets/images/tabbar/my_cases_icon.png"),
      page: MyCaseTab(),
    ),
    TabItem(
      tabName: "Messages",
      icon: AssetImage("assets/images/tabbar/msg_icon.png"),
      page: TimeLineTab(),
    ),
    TabItem(
      tabName: "Notifications",
      icon: AssetImage("assets/images/tabbar/noti_icon.png"),
      page: NotiTab(),
    ),
    TabItem(
      tabName: "More",
      icon: AssetImage("assets/images/tabbar/more_icon.png"),
      page: MoreTab(),
    )
  ];

  DashBoardScreenState() {
    // indexing is necessary for proper funcationality
    // of determining which tab is active
    tabs.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  // sets current tab index
  // and update state
  void _selectTab(int index) {
    if (index == currentTab) {
      // pop to first route
      // if the user taps on the active tab
      tabs[index].key.currentState.popUntil((route) => route.isFirst);
      //setState(() {});
    } else {
      // update the state
      // in order to repaint
      if (mounted) {
        setState(() => currentTab = index);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
      dialogHelp = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: WillPopScope(
          onWillPop: () async {
            final isFirstRouteInCurrentTab =
                !await tabs[currentTab].key.currentState.maybePop();
            if (isFirstRouteInCurrentTab) {
              // if not on the 'main' tab
              if (currentTab != 0) {
                // select 'main' tab
                _selectTab(0);
                // back button handled by app
                return false;
              }
            }
            // let system handle back button if we're on the first route
            return isFirstRouteInCurrentTab;
          },
          // this is the base scaffold
          // don't put appbar in here otherwise you might end up
          // with multiple appbars on one screen
          // eventually breaking the app
          child: Scaffold(
            // indexed stack shows only one child
            body: IndexedStack(
              index: currentTab,
              children: tabs.map((e) => e.page).toList(),
            ),
            // Bottom navigation
            bottomNavigationBar: BottomNavigation(
              context: context,
              onSelectTab: _selectTab,
              tabs: tabs,
              isHelpTut: (dialogHelp != null) ? true : false,
              totalMsg: 0,
              totalNoti: 0,
            ),
          ),
        ),
      ),
    );
  }
}
