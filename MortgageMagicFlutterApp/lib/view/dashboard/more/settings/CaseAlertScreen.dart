import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class CaseAlertScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Case alerts",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: drawCaseUI(context),
      ),
    );
  }

  drawCaseUI(context) {
    return Center(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 3)),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: "Shohokari.com says hi!",
                  txtColor: MyTheme.themeData.accentColor,
                  txtSize: 3,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Txt(
                  txt:
                      "If you are seeing this message, it means everything is ok!",
                  txtColor: MyTheme.themeData.accentColor,
                  txtSize: 2.5,
                  txtAlign: TextAlign.center,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Btn(
                txt: "Close",
                txtColor: Colors.white,
                bgColor: MyTheme.themeData.accentColor,
                width: getWP(context, 30),
                height: getHP(context, 8),
                isCurve: true,
                callback: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
