import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/mywidgets/BtnOutline.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:Mortgage_Magic/controller/helper/CommonHelper.dart';

class NotificationSettingsScreen extends StatefulWidget {
  @override
  State createState() => _NotificationSettingsState();
}

class _NotificationSettingsState extends State<NotificationSettingsScreen>
    with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Notification settings",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: drawTestUI(),
      ),
    );
  }

  drawTestUI() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: getHP(context, 3)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Txt(
                      txt: "Is it working?",
                      txtColor: Colors.black,
                      txtSize: 2.5,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: BtnOutline(
                    txt: "Test it",
                    txtColor: MyTheme.themeData.accentColor,
                    borderColor: Colors.transparent,
                    callback: () {
                      //
                    }),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt:
                    "Make sure you're actually getting those all important push notifications.",
                txtColor: Colors.grey,
                txtSize: 2.5,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          CommonHelper().getLine(context: context),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt:
                    "Your notifications can be updated at any time via the options below",
                txtColor: Colors.black,
                txtSize: 2.5,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          CommonHelper().getLine(context: context),
        ],
      ),
    );
  }
}
