import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/helper/more/MoreHelper.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'EditAccScreen.dart';
import 'NotificationSettingsScreen.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  List<String> listItems = ["Edit account", "Notification settings"];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Settings",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: Container(
          child: ListView.builder(
            itemCount: listItems.length,
            itemBuilder: (context, index) {
              final item = listItems[index];
              return GestureDetector(
                onTap: () async {
                  switch (index) {
                    case 0:
                      await Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (__) => EditAccScreen()));
                      break;
                    case 1:
                      await Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (__) => NotificationSettingsScreen()));
                      break;
                    default:
                  }
                },
                child: Card(
                  child: ListTile(
                    title: Txt(
                        txt: item,
                        txtColor: Colors.black,
                        txtSize: 2.5,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: (index != MoreHelper.listMore.length - 1)
                        ? Icon(
                            Icons.arrow_right,
                            color: Colors.grey,
                            size: 30,
                          )
                        : SizedBox(),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
