import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/controller/form_validate/UserProfileVal.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/InputBox.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:Mortgage_Magic/controller/helper/CommonHelper.dart';
import 'package:intl/intl.dart';

class EditAccScreen extends StatefulWidget {
  @override
  State createState() => _EditAccScreenState();
}

class _EditAccScreenState extends State<EditAccScreen> with Mixin {
  //  general info
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _location = TextEditingController();
  final _headline = TextEditingController();
  final _brief_bio = TextEditingController();

  //  private info
  final _email = TextEditingController();
  final _dob = TextEditingController();

  //  additional info
  final _mobile = TextEditingController();

  String dob = "";

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //  general info
    _fname.dispose();
    _lname.dispose();
    _location.dispose();
    _headline.dispose();
    _brief_bio.dispose();

    //  private info
    _email.dispose();
    _dob.dispose();

    //  additional info
    _mobile.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      final UserModel userModel = await DBMgr.shared.getUserProfile();
      //  general info
      _fname.text = userModel.firstName;
      _lname.text = userModel.lastName;
      _location.text = userModel.locations;
      _headline.text = userModel.headline;
      _brief_bio.text = userModel.briefBio;

      //  private info
      _email.text = userModel.email;
      _dob.text = userModel.dateofBirth;

      //  additional info
      _mobile.text = userModel.mobileNumber;
    } catch (e) {}
  }

  wsUpdateProfileAPI() async {
    try {
      if (validate()) {
        /*await NetworkMgr()
            .postData<LoginAPIModel, Null>(
          context: context,
          url: Server.LOGIN_URL,
          map: LoginHelper()
              .getParam(email: _email.text.trim(), pwd: _pwd.text.trim()),
          isCookie: true,
        )
            .then((model) async {
          if (model != null && mounted) {
            try {
              if (model.success) {
                //log(model.responseData.user.address);
                try {
                  //DBMgr.shared.getUserProfile();
                  //DBMgr.shared.setUserProfile(user: model.responseData.user);
                  
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  if (mounted) {
                    final err = model.errorMessages.login[0].toString();
                    showAlert(context: context, msg: err);
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        });*/
      }
    } catch (e) {
      log(e.toString());
    }
  }

  validate() {
    if (!UserProfileVal().isFNameOK(_fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(_lname)) {
      return false;
    }
    if (!UserProfileVal().isEmailOK(_email)) {
      return false;
    }
    if (!UserProfileVal().isPhoneOK(_mobile)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Edit profile",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),
          actions: <Widget>[
            FlatButton(
              textColor: Colors.white,
              onPressed: () {
                wsUpdateProfileAPI();
              },
              child: Text(
                "Save",
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
        ),
        body: Container(
          child: ListView(
            shrinkWrap: true,
            children: [
              drawGeneralInfoView(),
              drawPrivateInfoView(),
              drawAdditionalInfoView(),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Btn(
                  txt: "Save",
                  txtColor: Colors.white,
                  bgColor: MyTheme.themeData.accentColor,
                  width: getWP(context, 50),
                  height: getHP(context, 8),
                  isCurve: true,
                  callback: () {
                    wsUpdateProfileAPI();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawGeneralInfoView() {
    return Container(
      width: getW(context),
      child: Column(
        children: [
          Container(
            width: getW(context),
            color: Colors.blueGrey.shade100,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: "General information",
                  txtColor: Colors.black45,
                  txtSize: 2.5,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: _fname,
            lableTxt: "First Name",
            kbType: TextInputType.name,
            len: 20,
            isPwd: false,
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: _lname,
            lableTxt: "Last Name",
            kbType: TextInputType.name,
            len: 20,
            isPwd: false,
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: _location,
            lableTxt: "Location",
            kbType: TextInputType.streetAddress,
            len: 255,
            isPwd: false,
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: _headline,
            lableTxt: "Headline",
            kbType: TextInputType.text,
            len: 255,
            isPwd: false,
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: _brief_bio,
            lableTxt: "Brief Bio",
            kbType: TextInputType.text,
            len: 255,
            isPwd: false,
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  drawPrivateInfoView() {
    return Container(
      width: getW(context),
      child: Column(
        children: [
          Container(
            width: getW(context),
            color: Colors.blueGrey.shade100,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: "Private information",
                  txtColor: Colors.black45,
                  txtSize: 2.5,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: _email,
            lableTxt: "Email",
            kbType: TextInputType.emailAddress,
            len: 50,
            isPwd: false,
          ),
          SizedBox(height: 10),
          CommonHelper().getDOB(
            context: context,
            dob: dob,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    dob = DateFormat('dd-MMM-yyyy').format(value).toString();
                  } catch (e) {
                    log(e.toString());
                  }
                });
              }
            },
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  drawAdditionalInfoView() {
    return Container(
      width: getW(context),
      child: Column(
        children: [
          Container(
            width: getW(context),
            color: Colors.blueGrey.shade100,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                  txt: "Additional information",
                  txtColor: Colors.black45,
                  txtSize: 2.5,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
          SizedBox(height: 10),
          InputBox(
            ctrl: _mobile,
            lableTxt: "Current Mobile Number",
            kbType: TextInputType.phone,
            len: 20,
            isPwd: false,
          ),
          //SizedBox(height: 20),
        ],
      ),
    );
  }
}
