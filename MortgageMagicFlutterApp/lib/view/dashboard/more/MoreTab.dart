import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/helper/more/MoreHelper.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/view/dashboard/noti/NotiTab.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';

class MoreTab extends StatefulWidget {
  @override
  State createState() => _MoreTabState();
}

class _MoreTabState extends State<MoreTab> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "More",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        body: Container(
          child: ListView.builder(
            itemCount: MoreHelper.listMore.length,
            itemBuilder: (context, index) {
              Map<String, dynamic> mapMore = MoreHelper.listMore[index];
              return GestureDetector(
                onTap: () async {
                  if (mounted) {
                    Type route = mapMore['route'];
                    if (route != null) {
                      if (identical(route, NotiTab)) {
                        _stateProvider
                            .notify(ObserverState.STATE_CHANGED_tabbar4);
                      } else {
                        await MoreHelper().setRoute(
                            context: context,
                            route: route,
                            callback: (route2) {});
                      }
                    } else {
                      //  signout
                      _stateProvider.notify(ObserverState.STATE_CHANGED_logout);
                    }
                  }
                },
                child: Card(
                  child: ListTile(
                    title: Txt(
                        txt: mapMore['title'].toString(),
                        txtColor: Colors.black,
                        txtSize: 2.5,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: (index != MoreHelper.listMore.length - 1)
                        ? Icon(
                            Icons.arrow_right,
                            color: Colors.grey,
                            size: 30,
                          )
                        : SizedBox(),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
