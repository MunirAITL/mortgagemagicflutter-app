import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/view/dashboard/more/help/SupportCentreScreen.dart';
import 'package:Mortgage_Magic/view/dashboard/webview/WebScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class HelpScreen extends StatefulWidget {
  @override
  State createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> with Mixin {
  List<String> listItems = [
    "Support centre",
    "Terms and conditions",
    "Privacy",
    "Application tutorial"
  ];

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Help",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: Container(
          child: ListView.builder(
            itemCount: listItems.length,
            itemBuilder: (context, index) {
              final item = listItems[index];
              return GestureDetector(
                onTap: () async {
                  switch (index) {
                    case 0: //  Support Center
                      await Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (__) => SupportCentreScreen()));
                      break;
                    case 1: //  TC
                      await Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (__) => WebScreen(
                                    title: "Terms & Conditions",
                                    url: Server.TC_URL,
                                  )));
                      break;
                    case 2: //  Privacy
                      await Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (__) => WebScreen(
                                    title: "Privacy",
                                    url: Server.PRIVACY_URL,
                                  )));
                      break;
                    case 3: //  Application Tuturial
                      Navigator.pop(context);
                      _stateProvider
                          .notify(ObserverState.STATE_CHANGED_tabbar1);
                      break;
                    default:
                  }
                },
                child: Card(
                  child: ListTile(
                    title: Txt(
                        txt: item,
                        txtColor: Colors.black,
                        txtSize: 2.5,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: Icon(
                      Icons.arrow_right,
                      color: Colors.grey,
                      size: 30,
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
