import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/MediaUploadFilesAPIModel.dart';
import 'package:Mortgage_Magic/model/json/MediaUploadFilesModel.dart';
import 'package:Mortgage_Magic/model/json/ResolutionAPIModel.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:Mortgage_Magic/view/mywidgets/dropdown/DropListModel.dart';
import 'package:Mortgage_Magic/view/mywidgets/dropdown/SelectDropList.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:file_picker/file_picker.dart';
import 'package:dio/dio.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:jiffy/jiffy.dart';

class EmailScreen extends StatefulWidget {
  @override
  State createState() => _EmailScreenState();
}

class _EmailScreenState extends State<EmailScreen> with Mixin {
  final TextEditingController _desc = TextEditingController();

  static const int MAX_FILE_UPLOAD = 5;

  List<MediaUploadFilesModel> listMediaUploadFilesModel = [];

  UserModel userModel;

  //  dropdown
  DropListModel dropListModel = DropListModel([
    //"",
    OptionItem(id: "1", title: "Technical Problem with software"),
    OptionItem(id: "2", title: "I found a bug in the software"),
    OptionItem(id: "3", title: "I have a non-technical issue"),
    OptionItem(id: "4", title: "I have a Complain"),
    OptionItem(id: "5", title: "I have a question about My cases"),
  ]);
  OptionItem optionItemSelected =
      OptionItem(id: null, title: "Select Support Ticket Type");

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _desc.dispose();
    listMediaUploadFilesModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  wsMediaUploadFileAPI(File file) async {
    try {
      if (listMediaUploadFilesModel.length > MAX_FILE_UPLOAD) {
        showAlert(
            context: context,
            msg: "Maximum file upload limit is " +
                MAX_FILE_UPLOAD.toString() +
                " files");
        return;
      }

      await NetworkMgr().uploadFiles<MediaUploadFilesAPIModel, Null>(
        context: context,
        url: Server.MEDIA_UPLOADFILES_URL,
        files: [file],
      ).then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              listMediaUploadFilesModel.add(model.responseData.media);
              setState(() {});
            } else {
              final err = model.errorMessages.upload_videos[0];
              showAlert(context: context, msg: err);
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsResolutionAPI() async {
    try {
      if (optionItemSelected.id == null) {
        showAlert(
            context: context, msg: "Please choose ticket type from the list");
        return;
      } else if (_desc.text.trim().length == 0) {
        showAlert(context: context, msg: "Please enter description");
        return;
      }

      List<String> listFileUrl = [];
      for (MediaUploadFilesModel model in listMediaUploadFilesModel) {
        listFileUrl.add(model.url);
      }

      final param = {
        "Id": userModel.userCompanyInfo.userID,
        "Status": 101,
        "Title": optionItemSelected.title ?? '',
        "Description": _desc.text.trim(),
        "Remarks": "",
        "InitiatorId": 115765,
        "ServiceDate": Jiffy().format('dd-MM-yyyy'), //"05-Mar-2021",
        "ResolutionType": "Support Ticket",
        "ParentId": 0,
        "AssigneeId": 0,
        "UserCompanyId": userModel.userCompanyInfo.userID,
        "FileUrl": listFileUrl.join(','),
      };

      log(json.encode(param));
      await NetworkMgr()
          .postData<ResolutionAPIModel, Null>(
        context: context,
        url: Server.RESOLUTION_URL,
        param: param,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              _desc.text = "";
              listMediaUploadFilesModel.clear();
              final msg = model.messages.resolution_post[0];
              showAlert(context: context, msg: msg);
              setState(() {});
            } else {}
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  browseFiles() async {
    try {
      try {
        FilePickerResult result = await FilePicker.platform.pickFiles(
          allowMultiple: false,
          type: FileType.any,
          //type: FileType.custom,
          /*allowedExtensions: [
                      'jpg',
                      'jpeg',
                      'png',
                      'pdf',
                      'doc',
                      'docx'
                    ],*/
        );

        if (result != null) {
          wsMediaUploadFileAPI(File(result.files.single.path));
        }
      } catch (e) {
        log(e.toString());
      }
    } catch (e) {
      log(e.toString());
    }
  }

  appInit() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Contact us",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.send, color: Colors.white),
              onPressed: () {
                wsResolutionAPI();
              },
            ),
          ],
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawEmailUI(),
        ),
      ),
    );
  }

  drawEmailUI() {
    return ListView(
      shrinkWrap: true,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Txt(
              txt: "Support Ticket Type",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
          child: SelectDropList(
            this.optionItemSelected,
            this.dropListModel,
            (optionItem) {
              optionItemSelected = optionItem;
              setState(() {});
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Txt(
              txt: "How can we help you?",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        Container(
          margin: const EdgeInsets.only(left: 20.0, right: 20, top: 10),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(15))),
          child: TextField(
            controller: _desc,
            minLines: 5,
            maxLines: 10,
            //expands: true,
            autocorrect: false,
            maxLength: 500,
            keyboardType: TextInputType.multiline,
            style: TextStyle(
              color: Colors.black,
              fontSize: ResponsiveFlutter.of(context).fontSize(2),
            ),
            decoration: InputDecoration(
              hintText: 'Description',
              hintStyle: TextStyle(color: Colors.grey),
              //labelText: 'Your message',
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              contentPadding:
                  EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Txt(
              txt: "Attachments - " +
                  listMediaUploadFilesModel.length.toString() +
                  ' files added',
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        for (MediaUploadFilesModel model in listMediaUploadFilesModel)
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                    icon: Icon(
                      Icons.remove_circle,
                      color: Colors.red,
                    ),
                    onPressed: () {
                      listMediaUploadFilesModel.remove(model);
                      setState(() {});
                    }),
                Expanded(
                  child: Txt(
                      txt: model.name ?? '',
                      txtColor: MyTheme.themeData.accentColor,
                      txtSize: 2,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ],
            ),
          ),
        Padding(
          padding: const EdgeInsets.all(20),
          child: DottedBorder(
            borderType: BorderType.RRect,
            radius: Radius.circular(12),
            padding: EdgeInsets.all(6),
            color: Colors.grey,
            strokeWidth: 3,
            child: GestureDetector(
              onTap: () async {
                await browseFiles();
              },
              child: Container(
                height: getHP(context, 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.attach_file,
                      color: MyTheme.themeData.accentColor,
                      size: 30,
                    ),
                    Txt(
                        txt: "Add upto 5 files",
                        txtColor: Colors.black,
                        txtSize: 2,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ],
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Btn(
            txt: "Submit",
            txtColor: Colors.white,
            bgColor: MyTheme.themeData.accentColor,
            width: getW(context),
            height: getHP(context, 8),
            isCurve: true,
            callback: () {
              //
              wsResolutionAPI();
            },
          ),
        ),
        SizedBox(height: getHP(context, 5)),
      ],
    );
  }
}
