import 'package:Mortgage_Magic/config/AppDefine.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/dashboard/more/help/EmailScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:url_launcher/url_launcher.dart';

class SupportCentreScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Support centre",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: drawSupportButtons(context),
      ),
    );
  }

  drawSupportButtons(context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Email: " + AppDefine.SUPPORT_EMAIL,
              txtColor: Colors.white,
              bgColor: MyTheme.themeData.accentColor,
              width: getW(context),
              height: getHP(context, 8),
              isCurve: true,
              callback: () {
                launch("mailto:" + AppDefine.SUPPORT_EMAIL);
              },
            ),
          ),
          Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: 3,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Call: " + AppDefine.SUPPORT_CALL,
              txtColor: Colors.white,
              bgColor: MyTheme.themeData.accentColor,
              width: getW(context),
              height: getHP(context, 8),
              isCurve: true,
              callback: () {
                launch("tel://" + AppDefine.SUPPORT_CALL);
              },
            ),
          ),
          Txt(
              txt: "or",
              txtColor: Colors.black,
              txtSize: 3,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Btn(
              txt: "Send Message",
              txtColor: Colors.white,
              bgColor: MyTheme.themeData.accentColor,
              width: getW(context),
              height: getHP(context, 8),
              isCurve: true,
              callback: () async {
                await Navigator.push(context,
                    new MaterialPageRoute(builder: (__) => EmailScreen()));
              },
            ),
          ),
        ],
      ),
    );
  }
}
