import 'package:Mortgage_Magic/config/AppDefine.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class ReviewsScreen extends StatefulWidget {
  @override
  State createState() => _ReviewsScreenState();
}

class _ReviewsScreenState extends State<ReviewsScreen> with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Reviews",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),
        ),
        body: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: getWP(context, 50),
                  height: getHP(context, 30),
                  child: Image.asset(
                    'assets/images/screens/home/my_cases/case_nf.png',
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(height: 20),
                Txt(
                  txt: "There are no reviews to show!",
                  txtColor: Colors.black,
                  txtSize: 2.5,
                  txtAlign: TextAlign.center,
                  isBold: false,
                  txtLineSpace: 1.2,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
