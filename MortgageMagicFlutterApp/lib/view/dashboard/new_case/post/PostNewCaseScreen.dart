import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/config/dashboard/NewCaseCfg.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/controller/helper/CaseDetailsWebHelper.dart';
import 'package:Mortgage_Magic/controller/helper/PostNewCaseHelper.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/MortgageCaseInfoEntityModelListModel.dart';
import 'package:Mortgage_Magic/model/json/PostCaseAPIModel.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/dashboard/webview/WebScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:Mortgage_Magic/view/mywidgets/TxtBox.dart';
import 'package:Mortgage_Magic/view/mywidgets/com/OtherApplicantSwitchView.dart';
import 'package:Mortgage_Magic/view/mywidgets/com/SPVSwitchView.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class PostNewCaseScreen extends StatefulWidget {
  final int indexCase;
  const PostNewCaseScreen({
    Key key,
    @required this.indexCase,
  }) : super(key: key);
  @override
  State createState() => _PostNewCaseScreenState();
}

class _PostNewCaseScreenState extends State<PostNewCaseScreen>
    with Mixin, StateListener {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _note = TextEditingController();

  //  OtherApplicant input fields
  List<TextEditingController> listOApplicantInputFieldsCtr = [];

  //  SPVSwitchView input fields
  final _compName = TextEditingController();
  final _regAddr = TextEditingController();
  final _regNo = TextEditingController();
  String regDate = "";

  var title = "";

  bool isOtherApplicantSwitchShow = false;
  bool isSPVSwitchShow = false;
  bool isOtherApplicantSwitch = false;
  bool isSPVSwitch = false;
  int otherApplicantRadioIndex = 1;

  StateProvider _stateProvider;

  @override
  onStateChanged(ObserverState state) {
    if (state == ObserverState.STATE_CHANGED_otherapplicant) {
      isOtherApplicantSwitch = !isOtherApplicantSwitch;
      log("observer called 1");
    } else if (state == ObserverState.STATE_CHANGED_spvswitchview) {
      isSPVSwitch = !isSPVSwitch;
      log("observer called 2");
    }
  }

  wsOnPostCase() async {
    try {
      if (_note.text.trim().length == 0) {
        showAlert(context: context, msg: "Please enter case note");
        return;
      }

      final UserModel userModel = await DBMgr.shared.getUserProfile();
      final param = PostNewCaseHelper().getParam(
        isOtherApplicantSwitch: isOtherApplicantSwitch,
        isSPVSwitch: isSPVSwitch,
        compName: _compName.text.trim(),
        regAddr: _regAddr.text.trim(),
        regDate: regDate.trim(),
        regNo: _regNo.text.trim(),
        title: title.trim(),
        note: _note.text.trim(),
        userModel: userModel,
      );
      log(param);
      await NetworkMgr()
          .postData<PostCaseAPIModel, Null>(
        context: context,
        url: Server.POSTCASE_URL,
        param: param,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            final List<dynamic> mortgageCaseInfoEntityModelListModel =
                model.responseData.task.mortgageCaseInfoEntityModelListModel;
            int taskId = 0;
            try {
              if (mortgageCaseInfoEntityModelListModel.length > 0) {
                final MortgageCaseInfoEntityModelListModel model =
                    mortgageCaseInfoEntityModelListModel[0];
                taskId = model.taskID;
              }
            } catch (e) {
              log(e.toString());
            }
            await Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (__) => WebScreen(
                          title: "New Case - " + title,
                          url: CaseDetailsWebHelper().getLink(title, taskId),
                        ))).then((value) {
              try {
                Navigator.pop(context);
                _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar4);
              } catch (e) {
                log(e.toString());
              }
            });
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    _note.dispose();
    _compName.dispose();
    _regAddr.dispose();
    _regNo.dispose();
    listOApplicantInputFieldsCtr = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      if (Server.isTest) {
        _note.text = "xxxx";
      }
    } catch (e) {}
    _stateProvider = new StateProvider();
    _stateProvider.subscribe(this);
    try {
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
    } catch (e) {}
    try {
      final map = NewCaseCfg.listCreateNewCase[widget.indexCase];
      //final icon = map["url"];
      title = map["title"];
      if (map["isOtherApplicant"]) {
        isOtherApplicantSwitchShow = true;
      }
      if (map["isSPV"]) {
        isSPVSwitchShow = true;
      }
    } catch (e) {}
    try {
      _compName.addListener(() {
        log(_compName.text);
      });
      _regAddr.addListener(() {
        log(_regAddr.text);
      });
      _regNo.addListener(() {
        log(_regNo.text);
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "New Case",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            //height: getH(context),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: getHP(context, 5)),
                  drawCaseType(),
                  SizedBox(height: 20),
                  drawCaseNote(),
                  (isOtherApplicantSwitchShow)
                      ? OtherApplicantSwitchView(
                          listOApplicantInputFieldsCtr:
                              listOApplicantInputFieldsCtr,
                          callback: (value) {
                            otherApplicantRadioIndex = value;
                            log(value);
                          },
                        )
                      : SizedBox(),
                  (isSPVSwitchShow)
                      ? SPVSwitchView(
                          compName: _compName,
                          regAddr: _regAddr,
                          regNo: _regNo,
                          callback: (value) {
                            regDate = value;
                          },
                        )
                      : SizedBox(),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Btn(
                      txt: "Continue",
                      txtColor: Colors.white,
                      bgColor: MyTheme.themeData.accentColor,
                      width: getW(context),
                      height: getHP(context, 8),
                      isCurve: true,
                      callback: () async {
                        await wsOnPostCase();
                      },
                    ),
                  ),
                  SizedBox(height: getHP(context, 10)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  drawCaseType() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Type",
                txtColor: Colors.black,
                txtSize: 2.5,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 10),
            TxtBox(txt: title)
          ],
        ),
      ),
    );
  }

  drawCaseNote() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Note",
                txtColor: Colors.black,
                txtSize: 2.5,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 10),
            Container(
              //padding: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(15))),
              child: TextFormField(
                controller: _note,
                minLines: 5,
                maxLines: 10,
                autocorrect: false,
                keyboardType: TextInputType.multiline,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: ResponsiveFlutter.of(context).fontSize(2),
                ),
                decoration: new InputDecoration(
                  hintText: " Case Note",
                  hintStyle: new TextStyle(
                    color: Colors.grey,
                    fontSize: ResponsiveFlutter.of(context).fontSize(2),
                  ),
                  contentPadding: const EdgeInsets.symmetric(vertical: 0),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black, width: 3),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
