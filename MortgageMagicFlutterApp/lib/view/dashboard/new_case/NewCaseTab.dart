import 'package:Mortgage_Magic/config/AppConfig.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/dashboard/NewCaseCfg.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/controller/helper/CaseDetailsWebHelper.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/LocationsModel.dart';
import 'package:Mortgage_Magic/model/json/MortgageCaseInfoEntityModelListModel.dart';
import 'package:Mortgage_Magic/model/json/TaskInfoSearchAPIModel.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/dashboard/webview/WebScreen.dart';
import 'package:Mortgage_Magic/view/dashboard/new_case/post/PostNewCaseScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:Mortgage_Magic/view/mywidgets/com/PersistentHeader.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:mypkg/config/MyDefine.dart';
import 'package:mypkg/widgets/MyCacheImage.dart';
import 'package:badges/badges.dart';
import 'package:Mortgage_Magic/controller/helper/NewCaseHelper.dart';

class NewCaseTab extends StatefulWidget {
  @override
  State createState() => _NewCaseTabState();
}

class _NewCaseTabState extends State<NewCaseTab> with Mixin {
  List<dynamic> listTaskInfoSearchModel = [];

  bool isShowSlider = true;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit_dashboard;

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
    try {
      Future.delayed(Duration(seconds: 3 * NewCaseCfg.listSliderImages.length),
          () {
        if (mounted) {
          setState(() {
            isShowSlider = false;
          });
        }
      });
      wsTaskInformationBySearchAPI();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    listTaskInfoSearchModel = null;
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  wsTaskInformationBySearchAPI() async {
    try {
      setState(() {
        isLoading = true;
      });
      final UserModel userModel = await DBMgr.shared.getUserProfile();
      final url = NewCaseHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount + 1,
        userModel: userModel,
        status: NewCaseCfg.ALL.toString(),
      );
      log(url);
      await NetworkMgr()
          .getData<TaskInfoSearchAPIModel, Null>(
        context: context,
        url: url,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                listTaskInfoSearchModel = model.responseData.locations;
                log(listTaskInfoSearchModel.toString());
                if (listTaskInfoSearchModel.length > 0) {
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              } catch (e) {
                setState(() {
                  isLoading = false;
                });
                log(e.toString());
              }
            } else {
              setState(() {
                isLoading = false;
              });
            }
          } catch (e) {
            setState(() {
              isLoading = false;
            });
            log(e.toString());
          }
        }
      });
    } catch (e) {
      setState(() {
        isLoading = false;
      });
      log(e.toString());
    }
  }

  Future<void> _getRefreshData() async {
    //pageNo = 0;
    listTaskInfoSearchModel.clear();
    wsTaskInformationBySearchAPI();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "New Case",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverPersistentHeader(
              pinned: true,
              delegate: PersistentHeader(
                h: getHP(context, 15),
                widget: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Txt(
                      txt:
                          "Select the type of mortgage you are looking for from below and let Mortgage Magic do its magic",
                      txtColor: Colors.white,
                      txtSize: 2.5,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ),
            ),

            //  slider images
            drawSliderMMImages(),

            //  your recent cases
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Txt(
                      txt: "Your Recent Cases",
                      txtColor: MyTheme.themeData.accentColor,
                      txtSize: 3,
                      txtAlign: TextAlign.start,
                      isBold: true,
                    ),
                  ),
                  (listTaskInfoSearchModel.length == 0 && !isLoading)
                      ? GestureDetector(
                          onTap: () {
                            wsTaskInformationBySearchAPI();
                          },
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: Txt(
                                  txt: "Refresh",
                                  txtColor: Colors.grey,
                                  txtSize: 2,
                                  txtAlign: TextAlign.center,
                                  isBold: true),
                            ),
                          ),
                        )
                      : SizedBox(height: 20),
                ],
              ),
            ),

            drawRecentCases(),
            drawMoreLink(),
            //  create new case
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Txt(
                      txt: "Create a New Case",
                      txtColor: MyTheme.themeData.accentColor,
                      txtSize: 3,
                      txtAlign: TextAlign.start,
                      isBold: true,
                    ),
                  ),
                ],
              ),
            ),
            drawCreateNewCase(),
          ],
        ),
      ),
    );
  }

  drawSliderMMImages() {
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          (isShowSlider) ? drawGridImages() : SizedBox(),
        ],
      ),
    );
  }

  drawGridImages() {
    double h = 30;
    return Container(
        child: CarouselSlider(
      options: CarouselOptions(
        height: getHP(context, h),
        aspectRatio: 16 / 9,
        viewportFraction: 1,
        initialPage: 0,
        enableInfiniteScroll: true,
        reverse: false,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 3),
        autoPlayAnimationDuration: Duration(milliseconds: 800),
        autoPlayCurve: Curves.fastOutSlowIn,
        //enlargeCenterPage: true,
        //onPageChanged: callbackFunction,
        scrollDirection: Axis.horizontal,
      ),
      items: NewCaseCfg.listSliderImages
          .asMap()
          .map((i, element) => MapEntry(
              i,
              Container(
                width: getW(context),
                //margin: EdgeInsets.symmetric(horizontal: 0),
                //margin: EdgeInsets.symmetric(horizontal: 0),
                child: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: MyCacheImage(
                        url: element.toString() ?? MyDefine.MISSING_IMG,
                        width: getW(context),
                        height: h,
                        isCircle: false,
                      ),
                    ),
                    //Image.network(element, fit: BoxFit.cover)),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(200, 0, 0, 0),
                                Color.fromARGB(0, 0, 0, 0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20.0),
                          child: new DotsIndicator(
                            dotsCount: NewCaseCfg.listSliderImages.length,
                            position: double.parse(i.toString()),
                            decorator: DotsDecorator(
                              color: Colors.black, // Inactive color
                              activeColor: Colors.white,
                            ),
                          )),
                    ),
                  ],
                ),
              )))
          .values
          .toList(),
    ));
  }

  drawRecentCases() {
    final topBadgePos = getHP(context, 2);
    final endBadgePos = getHP(context, .05);
    return SliverList(
      delegate: SliverChildListDelegate(
        List<Widget>.generate(
          (listTaskInfoSearchModel.length > pageCount)
              ? pageCount
              : listTaskInfoSearchModel.length,
          (index) {
            if (mounted) {
              final LocationsModel model = listTaskInfoSearchModel[index];
              final icon =
                  NewCaseHelper().getCreateCaseIconByTitle(model.title);
              return (icon == null)
                  ? SizedBox()
                  : GestureDetector(
                      onTap: () async {
                        try {
                          final List<dynamic>
                              mortgageCaseInfoEntityModelListModel =
                              model.mortgageCaseInfoEntityModelListModel;
                          int taskId = 0;
                          try {
                            if (mortgageCaseInfoEntityModelListModel.length >
                                0) {
                              final MortgageCaseInfoEntityModelListModel model =
                                  mortgageCaseInfoEntityModelListModel[0];
                              taskId = model.taskID;
                            }
                          } catch (e) {
                            log(e.toString());
                          }
                          await Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (__) => WebScreen(
                                        title: "New Case - " + model.title,
                                        url: CaseDetailsWebHelper()
                                            .getLink(model.title, taskId),
                                      )));
                        } catch (e) {}
                      },
                      child: Container(
                        height: getHP(context, 25),
                        //color: Colors.blue,
                        child: Card(
                          color: Colors.white,
                          child: Row(
                            //contentPadding:
                            //EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
                            //dense: true,
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                width: getWP(context, 25),
                                //color: Colors.yellow,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: CircleAvatar(
                                        backgroundImage: AssetImage(icon),
                                        radius: 30,
                                        backgroundColor: Colors.transparent,
                                      ),
                                    ),
                                    SizedBox(height: 2),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Txt(
                                          txt: model.title,
                                          txtColor: Colors.black,
                                          txtSize: 1.8,
                                          txtAlign: TextAlign.center,
                                          isBold: false),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  width: double.infinity,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Txt(
                                          txt: model.title,
                                          txtColor:
                                              MyTheme.themeData.accentColor,
                                          txtSize: 2,
                                          txtAlign: TextAlign.center,
                                          isBold: true),
                                      SizedBox(height: 10),
                                      Container(
                                        color: NewCaseHelper()
                                            .getCaseStatusColor(
                                                model.caseStatus),
                                        width: double.infinity,
                                        child: Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: Txt(
                                              txt: model.status,
                                              txtColor: Colors.black,
                                              txtSize: 2,
                                              txtAlign: TextAlign.center,
                                              isBold: false),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Badge(
                                showBadge:
                                    (model.notificationUnreadTaskCount > 0)
                                        ? true
                                        : false,
                                badgeContent: Text(
                                  model.notificationUnreadTaskCount.toString(),
                                  //style: TextStyle(color: Colors.black),
                                ),
                                position: BadgePosition.topEnd(
                                    top: -topBadgePos, end: -endBadgePos),
                                child: Container(
                                  width: getWP(context, 14),
                                  height: getWP(context, 14),
                                  child: IconButton(
                                    icon: Image.asset(
                                      "assets/images/icons/case_msg_arrow_icon.png",
                                    ),
                                    onPressed: () {
                                      showToast(
                                          msg: "pending work...", which: 3);
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(width: 5),
                            ],
                          ),
                        ),
                      ),
                    );
            }
          },
        ),
      ),
    );
  }

  drawMoreLink() {
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          (listTaskInfoSearchModel.length > 0)
              ? Container(
                  width: getWP(context, 90),
                  child: GestureDetector(
                    onTap: () {
                      _stateProvider
                          .notify(ObserverState.STATE_CHANGED_tabbar2);
                    },
                    child: Text(
                      "See More...",
                      textAlign: TextAlign.end,
                      style: TextStyle(
                        color: MyTheme.themeData.accentColor,
                        fontSize: 20,
                      ),
                    ),
                  ),
                )
              : SizedBox()
        ],
      ),
    );
  }

  drawCreateNewCase() {
    int len = NewCaseCfg.listCreateNewCase.length;
    double w = getW(context);
    int crossAxisCount = 3;
    double boxW = w / crossAxisCount;
    double h = boxW * len / crossAxisCount;
    return SliverGrid(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount),
      delegate: SliverChildListDelegate(
        new List<Widget>.generate(
          len,
          (index) {
            final map = NewCaseCfg.listCreateNewCase[index];
            final icon = map["url"];
            final title = map["title"];
            log(title);
            return GestureDetector(
              onTap: () async {
                await Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (__) => PostNewCaseScreen(
                              indexCase: index,
                            ))).then((value) {
                  log("back to newcasetab");
                });
              },
              child: Container(
                //color: Colors.blue,
                height: boxW,
                width: boxW,
                child: Column(
                  children: [
                    Expanded(
                      child: CircleAvatar(
                        backgroundImage: AssetImage(icon),
                        radius: 30,
                        backgroundColor: Colors.transparent,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Txt(
                          txt: title,
                          txtColor: Colors.black,
                          txtSize: 1.5,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
