import 'package:Mortgage_Magic/config/AppConfig.dart';
import 'package:Mortgage_Magic/config/AppDefine.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/controller/helper/TimeLineHelper.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/TimeLineAPIModel.dart';
import 'package:Mortgage_Magic/model/json/TimeLineModel.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/dashboard/msg/chat/TaskBiddingScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:badges/badges.dart';

class TimeLineTab extends StatefulWidget {
  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends State<TimeLineTab> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  UserModel userModel;
  List<TimeLineModel> listTimeLineModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  wsOnPageLoad() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
      var url = TimeLineHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount,
        taskId: userModel.userCompanyInfo.entityID,
        isPrivate: true,
        customerId: 0,
        senderId: userModel.userCompanyInfo.userID,
        receiverId: '115773',
        timeLineId: '',
      ); //widget.model.entityId.toString());
      log(url);
      await NetworkMgr()
          .getData<TimeLineAPIModel, Null>(
        context: context,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> timeLines =
                    model.responseData.timelinePosts;

                if (timeLines != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (timeLines.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (TimeLineModel timeLine in timeLines) {
                      listTimeLineModel.add(timeLine);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listTimeLineModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showAlert(context: context, msg: "Timeline not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTimeLineModel.clear();
    wsOnPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
      wsOnPageLoad();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTimeLineModel = null;
    userModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Private Messages",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        body: Container(
          child: (listTimeLineModel.length > 0)
              ? NotificationListener(
                  onNotification: (scrollNotification) {
                    if (scrollNotification is ScrollStartNotification) {
                      //print('Widget has started scrolling');
                    } else if (scrollNotification is ScrollEndNotification) {
                      if (!isPageDone) {
                        pageStart++;
                        wsOnPageLoad();
                      }
                    }
                    return true;
                  },
                  child: RefreshIndicator(
                    onRefresh: _getRefreshData,
                    child: ListView.builder(
                      addAutomaticKeepAlives: true,
                      cacheExtent: AppConfig.page_limit.toDouble(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: listTimeLineModel.length,
                      itemBuilder: (context, index) {
                        if (mounted) {
                          return drawMessageList(index);
                        }
                      },
                    ),
                  ),
                )
              : (!isLoading)
                  ? Padding(
                      padding: const EdgeInsets.all(20),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: getWP(context, 50),
                              height: getHP(context, 30),
                              child: Image.asset(
                                'assets/images/screens/home/my_cases/case_nf.png',
                                fit: BoxFit.fill,
                              ),
                            ),
                            SizedBox(height: 20),
                            Txt(
                              txt:
                                  "Looks like you haven't created any timeline?",
                              txtColor: Colors.black,
                              txtSize: 2.5,
                              txtAlign: TextAlign.center,
                              isBold: false,
                              txtLineSpace: 1.2,
                            ),
                            SizedBox(height: 20),
                            GestureDetector(
                              onTap: () {
                                wsOnPageLoad();
                              },
                              child: Txt(
                                  txt: "Refresh",
                                  txtColor: MyTheme.themeData.accentColor,
                                  txtSize: 2,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                          ],
                        ),
                      ),
                    )
                  : SizedBox(),
        ),
      ),
    );
  }

  drawMessageList(int index) {
    try {
      final TimeLineModel model = listTimeLineModel[index];
      if (model == null) return SizedBox();
      final topBadgePos = getHP(context, 2);
      final endBadgePos = getHP(context, .05);

      return GestureDetector(
        onTap: () async {
          try {
            await Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (__) => TaskBiddingScreen(
                          taskId: model.taskId,
                          title: model.ownerName,
                        )));
          } catch (e) {}
        },
        child: Container(
          height: getHP(context, 20),
          //color: Colors.blue,
          child: Card(
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: getWP(context, 20),
                  //color: Colors.yellow,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundImage: NetworkImage(model.ownerImageUrl),
                        radius: 25,
                        backgroundColor: Colors.transparent,
                      ),
                      SizedBox(height: 2),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Txt(
                            txt: userModel.userCompanyInfo.entityName,
                            txtColor: Colors.black,
                            txtSize: 1.8,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    //color: Colors.yellow,
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: getWP(context, 30),
                          //color: Colors.blue,
                          child: Txt(
                              txt: model.ownerName,
                              txtColor: MyTheme.themeData.accentColor,
                              txtSize: 2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ),
                        SizedBox(width: 5),
                        CircleAvatar(
                          backgroundImage: NetworkImage(model.ownerImageUrl),
                          radius: 25,
                          backgroundColor: Colors.transparent,
                        ),
                        SizedBox(width: 5),
                        CircleAvatar(
                          backgroundImage: NetworkImage(model.ownerImageUrl),
                          radius: 25,
                          backgroundColor: Colors.transparent,
                        ),
                      ],
                    ),
                  ),
                ),
                Badge(
                  showBadge: (model.totalComments > 0) ? true : false,
                  badgeContent: Text(
                    model.totalComments.toString(),
                    //style: TextStyle(color: Colors.black),
                  ),
                  position: BadgePosition.topEnd(
                      top: -topBadgePos, end: -endBadgePos),
                  child: Container(
                    width: getWP(context, 14),
                    height: getWP(context, 14),
                    child: IconButton(
                      icon: Image.asset(
                        "assets/images/icons/case_msg_arrow_icon.png",
                      ),
                      onPressed: () {
                        showToast(msg: "pending work...", which: 3);
                      },
                    ),
                  ),
                ),
                SizedBox(width: 5),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
