import 'package:Mortgage_Magic/config/AppConfig.dart';
import 'package:Mortgage_Magic/config/AppDefine.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/controller/helper/TaskBiddingHelper.dart';
import 'package:Mortgage_Magic/model/json/TaskBiddingAPIModel.dart';
import 'package:Mortgage_Magic/model/json/TaskBiddingModel.dart';
import 'package:Mortgage_Magic/view/dashboard/msg/chat/ChatScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:badges/badges.dart';

class TaskBiddingScreen extends StatefulWidget {
  final String title;
  final int taskId;

  const TaskBiddingScreen({
    Key key,
    @required this.title,
    @required this.taskId,
  }) : super(key: key);
  @override
  State createState() => _TaskBiddingScreenState();
}

class _TaskBiddingScreenState extends State<TaskBiddingScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<TaskBiddingModel> listTaskBiddingModel = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  wsOnPageLoad() async {
    try {
      var url = TaskBiddingHelper().getUrl(taskId: widget.taskId.toString());
      log(url);
      await NetworkMgr()
          .getData<TaskBiddingAPIModel, Null>(
        context: context,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> taskBiddings =
                    model.responseData.taskBiddings;

                if (taskBiddings != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (taskBiddings.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (TaskBiddingModel taskBidding in taskBiddings) {
                      listTaskBiddingModel.add(taskBidding);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listTaskBiddingModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showAlert(context: context, msg: "TaskBiddings not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTaskBiddingModel.clear();
    wsOnPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
      wsOnPageLoad();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTaskBiddingModel = null;

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: widget.title,
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          child: RefreshIndicator(
            onRefresh: _getRefreshData,
            child: ListView.builder(
              itemCount: listTaskBiddingModel.length,
              itemBuilder: (context, index) {
                if (mounted) {
                  return drawMessageList(index);
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  drawMessageList(int index) {
    try {
      final topBadgePos = getHP(context, 2);
      final endBadgePos = getHP(context, .05);
      final TaskBiddingModel model = listTaskBiddingModel[index];
      return GestureDetector(
        onTap: () async {
          try {
            await Navigator.push(
                context, new MaterialPageRoute(builder: (__) => ChatScreen()));
          } catch (e) {}
        },
        child: Container(
          height: getHP(context, 20),
          //color: Colors.blue,
          child: Card(
            color: Colors.white,
            child: Row(
              //contentPadding:
              //EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
              //dense: true,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: getWP(context, 25),
                  //color: Colors.yellow,
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(model.ownerImageUrl),
                    radius: 40,
                    backgroundColor: Colors.transparent,
                  ),
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: model.ownerName,
                            txtColor: MyTheme.themeData.accentColor,
                            txtSize: 2.5,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        SizedBox(height: 10),
                        Txt(
                            txt: model.communityName,
                            txtColor: MyTheme.themeData.accentColor,
                            txtSize: 2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Badge(
                  showBadge: false, //(model.totalComments > 0) ? true : false,
                  badgeContent: Text(
                    model.totalComments.toString(),
                    //style: TextStyle(color: Colors.black),
                  ),
                  position: BadgePosition.topEnd(
                      top: -topBadgePos, end: -endBadgePos),
                  child: IconButton(
                    icon: Icon(
                      Icons.message,
                      color: MyTheme.themeData.accentColor,
                      size: 35,
                    ),
                    onPressed: () {
                      showToast(msg: "pending work...", which: 3);
                    },
                  ),
                ),
                SizedBox(width: 5),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
