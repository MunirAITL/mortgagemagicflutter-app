import 'dart:async';
import 'package:Mortgage_Magic/config/AppConfig.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/controller/helper/TimeLineMessagesHelper.dart';
import 'package:Mortgage_Magic/controller/helper/TimeLinePostHelper.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/TimeLineAPIModel.dart';
import 'package:Mortgage_Magic/model/json/TimeLineModel.dart';
import 'package:Mortgage_Magic/model/json/TimeLinePostAPIModel.dart';
import 'package:intl/intl.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/model/json/NotiModel.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mypkg/Mixin.dart';
import 'utils/ChatBubble.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class ChatScreen extends StatefulWidget {
  final NotiModel model;
  final UserModel userModel;
  final Map<String, dynamic> notiMap;

  const ChatScreen({
    Key key,
    @required this.model,
    @required this.notiMap,
    @required this.userModel,
  }) : super(key: key);
  @override
  State createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController textController = TextEditingController();
  ScrollController scrollController;

  UserModel userModel;
  List<TimeLineModel> listTimeLineModel = [];

  List<String> msgList = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  String startDate;

  wsPostTimeline() async {
    try {
      await NetworkMgr()
          .postData<TimeLinePostAPIModel, Null>(
        context: context,
        url: Server.TIMELINE_POST_URL,
        param: TimeLinePostHelper().getParam(),
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
            } else {
              try {
                if (mounted) {
                  final err = model.errorMessages.login[0].toString();
                  showAlert(context: context, msg: err);
                }
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsOnPageLoad() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
      }
      userModel = await DBMgr.shared.getUserProfile();
      var url = TimeLineMessagesHelper().getUrl(
        pageStart: pageStart,
        pageCount: pageCount,
        taskId: '70153', //userModel.userCompanyInfo.entityID,
        isPrivate: true,
        customerId: 0,
        senderId: '115767', //userModel.userCompanyInfo.userID,
        receiverId: '115773',
        timeLineId: '',
      ); //widget.model.entityId.toString());
      log(url);
      await NetworkMgr()
          .getData<TimeLineAPIModel, Null>(
        context: context,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> timeLines =
                    model.responseData.timelinePosts;

                if (timeLines != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (timeLines.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (TimeLineModel timeLine in timeLines) {
                      listTimeLineModel.add(timeLine);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listTimeLineModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showAlert(context: context, msg: "Timeline not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
      wsOnPageLoad();
    } catch (e) {}
  }

  @override
  void dispose() {
    listTimeLineModel = null;
    userModel = null;
    textController.dispose();
    scrollController = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      scrollController = ScrollController(initialScrollOffset: 0.0);
    } catch (e) {}
  }

  /// Scroll the Chat List when it goes to bottom
  _chatListScrollToBottom() {
    Timer(Duration(milliseconds: 100), () {
      if (scrollController.hasClients) {
        try {
          SchedulerBinding.instance.addPostFrameCallback((_) {
            scrollController.animateTo(
              scrollController.position.maxScrollExtent,
              duration: Duration(milliseconds: 100),
              curve: Curves.decelerate,
            );
          });
        } catch (e) {}
      }
    });
  }

  Widget buildSingleMessage({bool fromMe, msg}) {
    try {
      if (fromMe) {
        startDate = DateFormat('yyyy-MM-dd hh:mm').format(DateTime.now());
      }

      final curTime = DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now());
      print(curTime);

      Alignment alignment = fromMe ? Alignment.topRight : Alignment.topLeft;
      Alignment chatArrowAlignment =
          fromMe ? Alignment.topRight : Alignment.topLeft;
      TextStyle textStyle = TextStyle(
        fontSize: 16.0,
        color: fromMe ? Colors.white : Colors.white,
      );
      Color chatBgColor = fromMe ? Colors.green : Colors.blueGrey;
      EdgeInsets edgeInsets = fromMe
          ? EdgeInsets.fromLTRB(5, 5, 15, 5)
          : EdgeInsets.fromLTRB(15, 5, 5, 5);
      EdgeInsets margins = fromMe
          ? EdgeInsets.fromLTRB(80, 5, 10, 5)
          : EdgeInsets.fromLTRB(10, 5, 80, 5);

      return Row(
        children: [
          (!fromMe)
              ? getImage(
                  "https://mortgage-magic.co.uk/api/content/media/default_avatar.png")
              : SizedBox(),
          Expanded(
            child: Container(
              color: Colors.white,
              margin: margins,
              child: Align(
                alignment: alignment,
                child: Column(
                  children: <Widget>[
                    CustomPaint(
                      painter: ChatBubble(
                        color: chatBgColor,
                        alignment: chatArrowAlignment,
                      ),
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: Stack(
                          children: <Widget>[
                            Padding(
                              padding: edgeInsets,
                              child: Text(
                                msg.toString().trim(),
                                style: textStyle,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      getTimeAgoTxt(curTime),
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
              ),
            ),
          ),
          (fromMe)
              ? getImage(
                  "https://mortgage-magic.co.uk/api/content/media/default_avatar.png")
              : SizedBox(),
        ],
      );
    } catch (e) {
      return Container();
    }
  }

  getImage(url) => Image.network(
        url,
        width: 40,
        height: 40,
        //color: Colors.transparent,
      );
  Widget buildMessageList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            //print('Widget has started scrolling');
          } else if (scrollNotification is ScrollEndNotification) {
            if (!isPageDone) {
              pageStart++;
              wsOnPageLoad();
            }
          }
          return true;
        },
        child: ListView.builder(
          controller: scrollController,
          padding: new EdgeInsets.all(8.0),
          reverse: true,
          itemCount: msgList.length,
          itemBuilder: (BuildContext context, int index) {
            try {
              final msg = msgList[index];
              return buildSingleMessage(fromMe: true, msg: msg);
            } catch (e) {
              return Container();
            }
          },
        ),
      ),
    );
  }

  _bottomChatArea() {
    return Container(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: <Widget>[
          _chatTextArea(),
          IconButton(
            icon: Icon(
              Icons.send,
              color: Colors.green,
            ),
            onPressed: () async {
              //Check if the textfield has text or not
              onSendClicked();
            },
          ),
        ],
      ),
    );
  }

  onSendClicked() {
    if (textController.text.isNotEmpty) {
      //Add the message to the list
      DateTime now = DateTime.now();
      String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(now);
      if (mounted) {
        this.setState(() {
          msgList.add(textController.text.trim());
        });
        textController.text = '';
        //Scrolldown the list to show the latest message
        _chatListScrollToBottom();
      }
    }
  }

  _chatTextArea() {
    return Expanded(
      child: TextField(
        controller: textController,
        textInputAction: TextInputAction.send,
        onSubmitted: (value) {
          onSendClicked();
        },
        maxLength: 255,
        autocorrect: false,
        style: TextStyle(
          color: Colors.black,
          fontSize: ResponsiveFlutter.of(context).fontSize(2),
        ),
        decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.grey),
          counter: Offstage(),
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1,
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Colors.grey,
              width: 5,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(
              color: Colors.black,
              width: 0.0,
            ),
          ),
          filled: true,
          hintText: 'Type a message...',
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        //resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          title: Txt(
              txt: "Chat",
              txtColor: Colors.white,
              txtSize: 2.2,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawChatUI(),
        ),
      ),
    );
  }

  drawChatUI() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: <Widget>[
          buildMessageList(),
          //Divider(height: 1.0),
          _bottomChatArea(),
        ],
      ),
    );
  }
}
