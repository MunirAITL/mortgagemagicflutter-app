import 'package:Mortgage_Magic/config/AppDefine.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/model/json/NotiModel.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class NotiDetailsScreen extends StatefulWidget {
  final NotiModel model;
  final UserModel userModel;
  final Map<String, dynamic> notiMap;

  const NotiDetailsScreen({
    Key key,
    @required this.model,
    @required this.notiMap,
    @required this.userModel,
  }) : super(key: key);
  @override
  State createState() => _NotiDetailsScreenState();
}

class _NotiDetailsScreenState extends State<NotiDetailsScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  wsGetNotiDetails() async {}

  @override
  void initState() {
    super.initState();
    try {
      appInit();
      wsGetNotiDetails();
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 10,
            backgroundColor: MyTheme.themeData.accentColor,
            title: Txt(
                txt: "Notifications Details",
                txtColor: Colors.white,
                txtSize: 2.2,
                txtAlign: TextAlign.start,
                isBold: true),
            centerTitle: false,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              },
            ),
          ),
          body: drawNotiDetails()),
    );
  }

  drawNotiDetails() {
    return Container();
  }
}
