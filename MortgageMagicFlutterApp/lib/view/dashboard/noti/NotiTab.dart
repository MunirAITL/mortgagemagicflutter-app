import 'package:Mortgage_Magic/config/AppConfig.dart';
import 'package:Mortgage_Magic/config/AppDefine.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/config/dashboard/NotiCfg.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/controller/helper/NotiHelper.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/NotiAPIModel.dart';
import 'package:Mortgage_Magic/model/json/NotiModel.dart';
import 'package:Mortgage_Magic/model/json/UserModel.dart';
import 'package:Mortgage_Magic/view/mywidgets/BoldTxt.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';

class NotiTab extends StatefulWidget {
  @override
  State createState() => _NotiTabState();
}

class _NotiTabState extends State<NotiTab> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider = StateProvider();

  List<NotiModel> listNotiModel = [];
  UserModel userModel;

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  wsOnPageLoad() async {
    try {
      userModel = await DBMgr.shared.getUserProfile();
      final url = NotiHelper().getUrl(
          pageStart: pageStart, pageCount: pageCount, userModel: userModel);
      log(url);

      log(url);
      await NetworkMgr()
          .getData<NotiAPIModel, Null>(
        context: context,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                final List<dynamic> notifications =
                    model.responseData.notifications;

                if (notifications != null && mounted) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (notifications.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (NotiModel noti in notifications) {
                      listNotiModel.add(noti);
                    }
                  } catch (e) {
                    log(e.toString());
                  }
                  log(listNotiModel.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  if (mounted) {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                }
              } catch (e) {
                log(e.toString());
              }
            } else {
              try {
                //final err = model.errorMessages.login[0].toString();
                if (mounted) {
                  showAlert(context: context, msg: "Notifications not found");
                }
              } catch (e) {
                log(e.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            }
          } catch (e) {
            log(e.toString());
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        }
      });
    } catch (e) {
      log(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listNotiModel.clear();
    wsOnPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
      wsOnPageLoad();
    } catch (e) {}
  }

  @override
  void dispose() {
    listNotiModel = null;
    userModel = null;
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 10,
            backgroundColor: MyTheme.themeData.accentColor,
            title: Txt(
                txt: "Notifications",
                txtColor: Colors.white,
                txtSize: 2.2,
                txtAlign: TextAlign.start,
                isBold: true),
            centerTitle: false,
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: drawNotiList()),
    );
  }

  drawNotiList() {
    try {
      return Container(
        child: (listNotiModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      wsOnPageLoad();
                    }
                  }
                  return true;
                },
                child: RefreshIndicator(
                  onRefresh: _getRefreshData,
                  child: ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: listNotiModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      if (mounted) {
                        return drawNotiItem(index);
                      }
                    },
                  ),
                ),
              )
            : (!isLoading)
                ? Padding(
                    padding: const EdgeInsets.all(20),
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: getWP(context, 50),
                            height: getHP(context, 30),
                            child: Image.asset(
                              'assets/images/screens/home/my_cases/case_nf.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                          SizedBox(height: 20),
                          Txt(
                            txt:
                                "Looks like you haven't created any notification?",
                            txtColor: Colors.black,
                            txtSize: 2.5,
                            txtAlign: TextAlign.center,
                            isBold: false,
                            txtLineSpace: 1.2,
                          ),
                          SizedBox(height: 20),
                          GestureDetector(
                            onTap: () {
                              wsOnPageLoad();
                            },
                            child: Txt(
                                txt: "Refresh",
                                txtColor: MyTheme.themeData.accentColor,
                                txtSize: 2,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
      );
    } catch (e) {}
  }

  drawNotiItem(index) {
    try {
      NotiModel model = listNotiModel[index];
      if (model == null) return SizedBox();
      Map<String, dynamic> notiMap =
          NotiHelper().getNotiMap(model: model, userModel: userModel);
      if (notiMap.length == 0) return SizedBox();

      String txt = notiMap['txt'].toString();
      String eventName = '';
      if ((txt.endsWith(' ' + model.eventName))) {
        txt = txt.replaceAll(model.eventName.trim(), '');
        eventName = model.eventName;
      }

      return GestureDetector(
        onTap: () async {
          if (mounted) {
            await NotiHelper().setRoute(
                context: context,
                userModel: userModel,
                model: model,
                notiMap: notiMap,
                callback: () {
                  _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar3);
                });
          }
        },
        child: Container(
          //height: getHP(context, 25),
          //color: Colors.blue,
          child: Card(
            color: Colors.white,
            child: ListTile(
              leading: (model.initiatorImageUrl != '')
                  ? CircleAvatar(
                      backgroundImage: NetworkImage(model.initiatorImageUrl),
                      radius: 30,
                      backgroundColor: Colors.transparent,
                    )
                  : CircleAvatar(
                      child: Image.asset("assets/images/icons/user_icon.png"),
                      radius: 30,
                      backgroundColor: Colors.transparent,
                    ),
              title: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BoldTxt(text1: txt ?? '', text2: eventName),
                  SizedBox(height: 10),
                  Text(
                    notiMap['publishDateTime'].toString() ?? '',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 17,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
    return SizedBox();
  }
}
