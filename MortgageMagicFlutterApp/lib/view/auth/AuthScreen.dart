import 'dart:ui';
import 'package:Mortgage_Magic/config/AppDefine.dart';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/auth/LoginView.dart';
import 'package:Mortgage_Magic/view/auth/RegView.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:Mortgage_Magic/view/welcome/WelcomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool isRegView1 = true;
  @override
  Widget build(BuildContext context) {
    return (isRegView1)
        ? DefaultTabController(
            //  ******** On RegView1
            length: 2,
            child: SafeArea(
              child: Scaffold(
                key: _scaffoldKey,
                backgroundColor: Colors.white,
                appBar: PreferredSize(
                  preferredSize: Size.fromHeight(getHP(context, 20)),
                  child: AppBar(
                    elevation: 10,
                    backgroundColor: MyTheme.themeData.accentColor,
                    title: Txt(
                        txt: 'Join Mortgage Magic',
                        txtColor: Colors.white,
                        txtSize: 2,
                        txtAlign: TextAlign.center,
                        isBold: true),
                    centerTitle: true,
                    leading: IconButton(
                        icon: Icon(Icons.arrow_back, color: Colors.white),
                        onPressed: () async {
                          await Navigator.pushReplacement(
                              context,
                              new MaterialPageRoute(
                                  builder: (__) => WelcomeScreen()));
                        }),
                    bottom: TabBar(
                      //labelColor: Colors.deepOrange,
                      unselectedLabelColor: Colors.grey,
                      indicatorColor: Colors.black,
                      indicatorSize: TabBarIndicatorSize.tab,
                      tabs: [
                        Container(
                            height: getHP(context, 7),
                            child: Txt(
                              txt: "CREATE AN ACCOUNT",
                              txtColor: Colors.white,
                              txtSize: 1.8,
                              txtAlign: TextAlign.center,
                              isBold: false,
                            )),
                        Container(
                            height: getHP(context, 7),
                            child: Txt(
                              txt: "LOG IN",
                              txtColor: Colors.white,
                              txtSize: 1.8,
                              txtAlign: TextAlign.center,
                              isBold: false,
                            )),
                      ],
                    ),
                  ),
                ),
                body: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onPanDown: (detail) {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  onTap: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: TabBarView(
                    children: [
                      RegView(
                        notifyParent: () {
                          if (mounted) {
                            setState(() {
                              isRegView1 = false;
                            });
                          }
                        },
                        isRegView1: isRegView1,
                      ),
                      LoginView(),
                    ],
                  ),
                ),
              ),
            ),
          )
        : SafeArea(
            //  ******** On RegView2
            child: Scaffold(
              key: _scaffoldKey,
              appBar: AppBar(
                elevation: 10,
                backgroundColor: MyTheme.themeData.accentColor,
                title: Txt(
                    txt: 'Create Profile',
                    txtColor: Colors.white,
                    txtSize: 2,
                    txtAlign: TextAlign.center,
                    isBold: true),
                centerTitle: false,
                leading: IconButton(
                    icon: Icon(Icons.arrow_back, color: Colors.white),
                    onPressed: () {
                      setState(() {
                        isRegView1 = true;
                      });
                    }),
              ),
              body: RegView(
                notifyParent: () {
                  if (mounted) {
                    setState(() {
                      isRegView1 = false;
                    });
                  }
                },
                isRegView1: isRegView1,
              ),
            ),
          );
  }
}
