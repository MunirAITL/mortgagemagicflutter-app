import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/controller/form_validate/UserProfileVal.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/LoginAPIModel.dart';
import 'package:Mortgage_Magic/view/auth/ForgotDialog.dart';
import 'package:Mortgage_Magic/view/auth/sms/Sms1Screen.dart';
import 'package:Mortgage_Magic/view/dashboard/DashboardScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/BtnLoginBy.dart';
import 'package:Mortgage_Magic/view/mywidgets/InputBox.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:Mortgage_Magic/controller/helper/LoginHelper.dart';

class LoginView extends StatefulWidget {
  @override
  State createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> with Mixin {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pwd = TextEditingController();

  var accentColor;
  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      if (Server.isTest) {
        _email.text = "tah12@yopmail.com";
        _pwd.text = "123";
      }
      accentColor = MyTheme.themeData.accentColor;
    } catch (e) {}
  }

  wsLoginAPI() async {
    try {
      if (validate()) {
        await NetworkMgr()
            .postData<LoginAPIModel, Null>(
          context: context,
          url: Server.LOGIN_URL,
          param: LoginHelper()
              .getParam(email: _email.text.trim(), pwd: _pwd.text.trim()),
        )
            .then((model) async {
          if (model != null && mounted) {
            try {
              if (model.success) {
                //log(model.responseData.user.address);
                try {
                  //DBMgr.shared.getUserProfile();
                  DBMgr.shared.setUserProfile(user: model.responseData.user);
                  await Navigator.pushReplacement(
                      context,
                      new MaterialPageRoute(
                          builder: (__) => DashBoardScreen()));
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  if (mounted) {
                    final err = model.errorMessages.login[0].toString();
                    showAlert(context: context, msg: err);
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        });
      }
    } catch (e) {
      log(e.toString());
    }
  }

  wsForgotAPI(emailStr) {}

  validate() {
    if (!UserProfileVal().isNotEmpty(_email, 'Email/Phone')) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: getHP(context, 5)),
              Container(
                child: Column(
                  children: [
                    InputBox(
                      ctrl: _email,
                      lableTxt: "Email or Phone Number",
                      kbType: TextInputType.emailAddress,
                      len: 50,
                      isPwd: false,
                    ),
                    SizedBox(height: 10),
                    InputBox(
                      ctrl: _pwd,
                      lableTxt: "Password",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true,
                    ),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                            onTap: () {
                              showForgotDialog(
                                  context: context,
                                  email: _email,
                                  callback: (String emailStr) {
                                    //
                                    if (emailStr != null) {
                                      if (emailStr.length > 0) {
                                        wsForgotAPI(emailStr);
                                      }
                                    }
                                  });
                            },
                            child: Txt(
                                txt: "Forgot Password?",
                                txtColor: MyTheme.themeData.accentColor,
                                txtSize: 2,
                                txtAlign: TextAlign.center,
                                isBold: true)),
                        SizedBox(width: 10),
                        Btn(
                          txt: "Login",
                          txtColor: Colors.white,
                          bgColor: accentColor,
                          width: getWP(context, 30),
                          height: getHP(context, 8),
                          isCurve: true,
                          callback: () {
                            wsLoginAPI();
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Txt(
                txt: "or",
                txtColor: Colors.black,
                txtSize: 2.5,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Btn(
                  txt: "Log in with Mobile",
                  txtColor: Colors.white,
                  bgColor: MyTheme.themeData.accentColor,
                  width: getW(context),
                  height: getHP(context, 8),
                  isCurve: false,
                  callback: () async {
                    await Navigator.push(context,
                        new MaterialPageRoute(builder: (__) => Sms1Screen()));
                  },
                ),
              ),
              SizedBox(height: getHP(context, 10)),
            ],
          ),
        ),
      ),
    );
  }
}
