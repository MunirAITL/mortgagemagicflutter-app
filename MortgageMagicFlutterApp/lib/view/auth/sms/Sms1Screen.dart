import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/view/auth/sms/OtpScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/IcoTxtIco.dart';
import 'package:Mortgage_Magic/view/mywidgets/TCView.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'Sms2Screen.dart';

class Sms1Screen extends StatefulWidget {
  @override
  State createState() => _Sms1ScreenState();
}

class _Sms1ScreenState extends State<Sms1Screen> with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        //resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(height: getHP(context, 5)),
            Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Txt(
                txt: "Please give your mobile number to start using the app",
                txtColor: Colors.white,
                txtSize: 2.5,
                txtAlign: TextAlign.center,
                txtLineSpace: 1.2,
                isBold: false,
              ),
            ),
            drawCenterMobileCode(),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: TCView(
                screenName: 'Log in',
                txt1Color: Colors.white,
                txt2Color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawCenterMobileCode() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        height: getHP(context, 45),
        width: getWP(context, 80),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(20.0),
            topRight: const Radius.circular(20.0),
            bottomLeft: const Radius.circular(20.0),
            bottomRight: const Radius.circular(20.0),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Txt(
                txt: "Please give your mobile number",
                txtColor: MyTheme.themeData.accentColor,
                txtSize: 2,
                txtAlign: TextAlign.center,
                isBold: true,
              ),
            ),
            GestureDetector(
              onTap: () async {
                await Navigator.push(context,
                    new MaterialPageRoute(builder: (__) => Sms2Screen()));
                /*await Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (__) => OtpScreen(
                              verificationId: '',
                              sec: 60,
                              phoneNumber: '+923112824995',
                            )));*/
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: IcoTxtIco(
                  leftIcon: Icons.mobile_screen_share,
                  txt: "Log in with Mobile",
                  rightIcon: Icons.arrow_right,
                  iconColor: MyTheme.themeData.accentColor,
                  leftIconSize: 40,
                  rightIconSize: 50,
                ),
              ),
            ),
            SizedBox(height: getHP(context, 5)),
            Expanded(
              child: Container(
                width: getWP(context, 85),
                //height: getHP(context, 20),
                child: Image.asset(
                  'assets/images/screens/login/login_mobile_bg.png',
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
