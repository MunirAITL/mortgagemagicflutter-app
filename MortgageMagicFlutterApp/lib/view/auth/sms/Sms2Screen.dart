import 'dart:async';
import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/controller/form_validate/UserProfileVal.dart';
import 'package:Mortgage_Magic/view/dashboard/DashboardScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mypkg/Mixin.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

import 'OtpScreen.dart';
//import 'package:responsive_flutter/responsive_flutter.dart';

class Sms2Screen extends StatefulWidget {
  @override
  State createState() => _Sms2ScreenState();
}

class _Sms2ScreenState extends State<Sms2Screen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _phoneController = TextEditingController();

  Color btnColor;
  bool isOtpCalled = false;
  bool isLoading = false;
  final int timeOut = 60;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _phoneController.dispose();
    try {
      stopLoading();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _phoneController.text = "+923112824994";
      btnColor = Colors.grey[300];
      _phoneController.addListener(() {
        if (_phoneController.text.trim().length < UserProfileVal.PHONE_LIMIT) {
          btnColor = Colors.grey[300];
        } else {
          btnColor = MyTheme.themeData.accentColor;
        }
        if (mounted) {
          setState(() {});
        }
      });
    } catch (e) {}
  }

  Future<bool> codeSendClicked() async {
    try {
      if (!isLoading)
        startLoading();
      else
        stopLoading();
      final _auth = FirebaseAuth.instance;
      _auth.verifyPhoneNumber(
        phoneNumber: _phoneController.text,
        timeout: Duration(seconds: timeOut),
        verificationCompleted: (AuthCredential credential) async {
          stopLoading();
          final User user = (await _auth.signInWithCredential(credential)).user;
          if (user != null) {
            //	next screen
            await Navigator.pushReplacement(context,
                new MaterialPageRoute(builder: (__) => DashBoardScreen()));
          }
        },
        verificationFailed: (FirebaseAuthException exception) {
          stopLoading();
          showToast(msg: "Failed to verify");
          print(
              'Phone number verification failed. Code: ${exception.code}. Message: ${exception.message}');
        },
        codeSent: (String verificationId, [int forceResendingToken]) async {
          //	design UI to take code
          stopLoading();
          gotoOtpScreen(verificationId);
        },
        codeAutoRetrievalTimeout: (String verificationId) async {
          stopLoading();
          if (!isOtpCalled) {
            gotoOtpScreen(verificationId);
          }
        },
      );
    } catch (e) {
      stopLoading();
    }
  }

  gotoOtpScreen(verificationId) async {
    try {
      isOtpCalled = true;
      await Navigator.push(
        context,
        new MaterialPageRoute(
          builder: (__) => OtpScreen(
            verificationId: verificationId,
            sec: timeOut,
            phoneNumber: _phoneController.text.trim(),
          ),
        ),
      ).then((value) {
        if (mounted) {
          setState(() {
            isOtpCalled = false;
            _phoneController.text = value;
          });
        }
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 10,
          backgroundColor: MyTheme.themeData.accentColor,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: Container(
          width: getW(context),
          //height: getH(context),
          child: drawSendBox(),
        ),
      ),
    );
  }

  drawSendBox() {
    return Form(
      child: Column(
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: getHP(context, 5)),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Enter your mobile number",
              txtColor: Colors.black,
              txtSize: 2.5,
              txtAlign: TextAlign.center,
              txtLineSpace: 1.0,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: new TextFormField(
              controller: _phoneController,
              keyboardType: TextInputType.phone,
              maxLength: 15,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                color: Colors.black,
              ),
              decoration: new InputDecoration(
                hintStyle: new TextStyle(color: Colors.grey, fontSize: 24),
                hintText: "+44 xxxxxx",
                focusedBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.black),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
                border: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.black),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
                enabledBorder: new OutlineInputBorder(
                  borderSide: new BorderSide(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(20.0),
                  ),
                ),
              ),
            ),
          ),
          Btn(
            txt: "Next",
            txtColor: Colors.black,
            bgColor: btnColor,
            width: getWP(context, 80),
            height: getHP(context, 8),
            isCurve: false,
            callback: () async {
              if (_phoneController.text.trim().length >=
                  UserProfileVal.PHONE_LIMIT) {
                //  Firebase phone auth call for sending sms code
                //_phoneNumberController.text = await _autoFill.hint;

                codeSendClicked();
              } else {
                showToast(msg: 'Please enter your valid phone number');
              }
            },
          ),
        ],
      ),
    );
  }
}
