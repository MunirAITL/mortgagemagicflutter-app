import 'package:Mortgage_Magic/config/MyTheme.dart';
import 'package:Mortgage_Magic/config/Server.dart';
import 'package:Mortgage_Magic/controller/form_validate/UserProfileVal.dart';
import 'package:Mortgage_Magic/controller/network/NetworkMgr.dart';
import 'package:Mortgage_Magic/controller/observer/StateProvider.dart';
import 'package:Mortgage_Magic/model/db/DBMgr.dart';
import 'package:Mortgage_Magic/model/json/RegAPIModel.dart';
import 'package:Mortgage_Magic/view/dashboard/DashboardScreen.dart';
import 'package:Mortgage_Magic/view/mywidgets/Btn.dart';
import 'package:Mortgage_Magic/view/mywidgets/BtnLoginBy.dart';
import 'package:Mortgage_Magic/view/mywidgets/InputBox.dart';
import 'package:Mortgage_Magic/view/mywidgets/TCView.dart';
import 'package:Mortgage_Magic/view/mywidgets/Txt.dart';
import 'package:Mortgage_Magic/view/mywidgets/com/CompSwitchView.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:intl/intl.dart';
import 'package:random_string/random_string.dart';
import 'package:Mortgage_Magic/controller/helper/RegHelper.dart';
import 'package:Mortgage_Magic/controller/helper/CommonHelper.dart';

class RegView extends StatefulWidget {
  final Function()
      notifyParent; //for changing status bar and tab hide and show on next regview2
  bool isRegView1;
  RegView({Key key, @required this.notifyParent, @required this.isRegView1})
      : super(key: key);
  @override
  State createState() => _RegViewState();
}

enum RadioEnum { male, female }

class _RegViewState extends State<RegView> with Mixin, StateListener {
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _phone = TextEditingController();
  final _mobile = TextEditingController();
  final _pwd = TextEditingController();
  final _compName = TextEditingController();

  String dob = "";
  String dobDD = "";
  String dobMM = "";
  String dobYY = "";
  var accentColor;
  RadioEnum _character = RadioEnum.male;

  bool isSwitch = true;

  StateProvider _stateProvider;

  @override
  onStateChanged(ObserverState state) {
    if (state == ObserverState.STATE_CHANGED) {
      isSwitch = !isSwitch;
      //setState(() {});
      log("observer called");
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    _email.dispose();
    _pwd.dispose();
    _fname.dispose();
    _lname.dispose();
    _phone.dispose();
    _mobile.dispose();
    _compName.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    if (Server.isTest) {
      _fname.text = "Munir";
      _lname.text = "Abbas";
      _email.text = randomAlphaNumeric(5).toString() + "@gmail.com";
      _phone.text = randomNumeric(15).toString();
      _mobile.text = _phone.text;
      _pwd.text = "123";
      _compName.text = (isSwitch) ? "aitl" : '';
    }
    _stateProvider = new StateProvider();
    _stateProvider.subscribe(this);
    accentColor = MyTheme.themeData.accentColor;
    try {
      _compName.addListener(() {
        log(_compName.text);
      });
    } catch (e) {}
  }

  wsRegAPI() async {
    try {
      if (validate2()) {
        final param = RegHelper().getParam(
          email: _email.text.trim(),
          pwd: _pwd.text.trim(),
          fname: _fname.text.trim(),
          lname: _lname.text.trim(),
          phone: _phone.text.trim(),
          dob: dob,
          dobDD: dobDD,
          dobMM: dobMM,
          dobYY: dobYY,
        );
        await NetworkMgr()
            .postData<RegAPIModel, Null>(
          context: context,
          url: Server.REG_URL,
          param: param,
        )
            .then((model) async {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  DBMgr.shared.setUserProfile(
                    user: model.responseData.user,
                    otpID: model.responseData.otpId.toString(),
                    otpMobileNumber: model.responseData.otpMobileNumber,
                  );
                  await Navigator.pushReplacement(
                      context,
                      new MaterialPageRoute(
                          builder: (__) => DashBoardScreen()));
                } catch (e) {
                  log(e.toString());
                }
              } else {
                try {
                  if (mounted) {
                    final err = model.errorMessages.register[0].toString();
                    showAlert(context: context, msg: err);
                  }
                } catch (e) {
                  log(e.toString());
                }
              }
            } catch (e) {
              log(e.toString());
            }
          }
        });
      }
    } catch (e) {
      log(e.toString());
    }
  }

  validate1() {
    if (!UserProfileVal().isFNameOK(_fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(_lname)) {
      return false;
    } else if (!UserProfileVal().isEmailOK(_email)) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(_phone)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(_pwd)) {
      return false;
    } else if (isSwitch && !UserProfileVal().isComNameOK(_compName)) {
      return false;
    }
    return true;
  }

  validate2() {
    if (!UserProfileVal().isDOBOK(dob)) {
      return false;
    } else if (isSwitch && !UserProfileVal().isComNameOK(_compName)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return (widget.isRegView1) ? drawRegView1() : drawRegView2();
  }

  //  ***************** RegView1
  drawRegView1() {
    return Container(
      width: getW(context),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getHP(context, 5)),
            Container(
              child: Column(
                children: [
                  InputBox(
                    ctrl: _fname,
                    lableTxt: "First name",
                    kbType: TextInputType.text,
                    len: 20,
                    isPwd: false,
                  ),
                  SizedBox(height: 10),
                  InputBox(
                    ctrl: _lname,
                    lableTxt: "Last name",
                    kbType: TextInputType.text,
                    len: 20,
                    isPwd: false,
                  ),
                  SizedBox(height: 10),
                  InputBox(
                    ctrl: _email,
                    lableTxt: "Email",
                    kbType: TextInputType.emailAddress,
                    len: 50,
                    isPwd: false,
                  ),
                  SizedBox(height: 10),
                  InputBox(
                    ctrl: _phone,
                    lableTxt: "Phone Number",
                    kbType: TextInputType.phone,
                    len: 20,
                    isPwd: false,
                  ),
                  SizedBox(height: 10),
                  InputBox(
                    ctrl: _pwd,
                    lableTxt: "Password",
                    kbType: TextInputType.text,
                    len: 20,
                    isPwd: true,
                  ),
                  //SizedBox(height: 20),
                  CompSwitchView(compName: _compName),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Btn(
                      txt: "Create an account",
                      txtColor: Colors.white,
                      bgColor: accentColor,
                      width: getW(context),
                      height: getHP(context, 8),
                      isCurve: false,
                      callback: () async {
                        if (validate1()) {
                          widget.isRegView1 = false;
                          widget.notifyParent();
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
            SizedBox(height: 10),
            /*BtnLoginBy(
              callbackLoginMobile: null,
              callbackFBLogin: () {},
              callbackGLogin: () {},
            ),*/
            Padding(
              padding: const EdgeInsets.all(20),
              child: TCView(screenName: 'Log in'),
            ),
          ],
        ),
      ),
    );
  }

  //  ***************** RegView2  **********************

  drawRegView2() {
    return Container(
      width: getW(context),
      height: getH(context),
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: getHP(context, 5)),
            drawMobile(),
            SizedBox(height: 20),
            CommonHelper().getDOB(
              context: context,
              dob: dob,
              callback: (value) {
                if (mounted) {
                  setState(() {
                    try {
                      dob = DateFormat('dd-MMM-yyyy').format(value).toString();
                      final dobArr = dob.toString().split('-');
                      this.dobDD = dobArr[0];
                      this.dobMM = dobArr[1];
                      this.dobYY = dobArr[2];
                    } catch (e) {
                      log(e.toString());
                    }
                  });
                }
              },
            ),
            SizedBox(height: 20),
            drawGender(),
            SizedBox(height: 20),
            CompSwitchView(compName: _compName),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Btn(
                txt: "Get Started",
                txtColor: Colors.white,
                bgColor: accentColor,
                width: getW(context),
                height: getHP(context, 8),
                isCurve: true,
                callback: () {
                  wsRegAPI();
                },
              ),
            ),
            SizedBox(height: getHP(context, 10)),
          ],
        ),
      ),
    );
  }

  drawMobile() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Current mobile number? *",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
          ),
          SizedBox(height: 5),
          InputBox(
            ctrl: _mobile,
            lableTxt: "Mobile Number",
            kbType: TextInputType.phone,
            len: 20,
            isPwd: false,
          ),
        ],
      ),
    );
  }

  drawGender() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "Gender",
              txtColor: Colors.black,
              txtSize: 2,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
          ),
          SizedBox(height: 5),
          Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            color: Colors.white,
            child: Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: Colors.black,
                disabledColor: Colors.black,
                selectedRowColor: Colors.black,
                indicatorColor: Colors.black,
                toggleableActiveColor: Colors.black,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _character = RadioEnum.male;
                        });
                      }
                    },
                    child: Container(
                      width: getWP(context, 50),
                      child: ListTile(
                        title: Align(
                          alignment: Alignment(-1.2, 0),
                          child: Txt(
                            txt: "Male",
                            txtColor: Colors.black,
                            txtSize: 2,
                            txtAlign: TextAlign.center,
                            isBold: false,
                          ),
                        ),
                        leading: Radio(
                          value: RadioEnum.male,
                          groupValue: _character,
                          onChanged: (RadioEnum value) {
                            if (mounted) {
                              setState(() {
                                _character = value;
                              });
                            }
                          },
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        if (mounted) {
                          setState(() {
                            _character = RadioEnum.female;
                          });
                        }
                      },
                      child: Container(
                        width: getWP(context, 50),
                        //color: Colors.black,
                        child: ListTile(
                          title: Align(
                            alignment: Alignment(-1.2, 0),
                            child: Txt(
                              txt: "Female",
                              txtColor: Colors.black,
                              txtSize: 2,
                              txtAlign: TextAlign.center,
                              isBold: false,
                            ),
                          ),
                          leading: Radio(
                            value: RadioEnum.female,
                            groupValue: _character,
                            onChanged: (RadioEnum value) {
                              if (mounted) {
                                setState(() {
                                  _character = value;
                                });
                              }
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
