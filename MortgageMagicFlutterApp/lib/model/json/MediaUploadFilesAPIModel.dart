import 'MediaUploadFilesModel.dart';

class MediaUploadFilesAPIModel {
  bool success;
  _ErrorMessages errorMessages;
  dynamic messages;
  _ResponseData responseData;

  MediaUploadFilesAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory MediaUploadFilesAPIModel.fromJson(Map<String, dynamic> j) {
    return MediaUploadFilesAPIModel(
      success: j['Success'] as bool,
      errorMessages: _ErrorMessages.fromJson(j['ErrorMessages']) ?? null,
      messages: j['Messages'],
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {
  List<dynamic> upload_videos;
  _ErrorMessages({this.upload_videos});
  factory _ErrorMessages.fromJson(Map<String, dynamic> j) {
    return _ErrorMessages(
      upload_videos: j['upload_videos'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'upload_videos': upload_videos,
      };
}

class _ResponseData {
  MediaUploadFilesModel media;
  _ResponseData({this.media});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    return _ResponseData(
        media: (j['Media'] != null)
            ? MediaUploadFilesModel.fromJson(j['Media'])
            : null);
  }
  Map<String, dynamic> toMap() => {
        'Media': media,
      };
}
