class TimeLinePostAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  TimeLinePostAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});
  //LoginModel({this.success, this.errorMessages, this.messages});

  factory TimeLinePostAPIModel.fromJson(Map<String, dynamic> j) {
    return TimeLinePostAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null) ? j['ResponseData'] : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ResponseData {}
