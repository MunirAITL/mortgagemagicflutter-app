import 'UserCommentPublicModelListModel.dart';

class TimeLineModel {
  int id;
  int ownerId;
  String ownerEntityType;
  String ownerImageUrl;
  String ownerProfileUrl;
  String ownerName;
  String postTypeName;
  bool isSponsored;
  String message;
  String additionalAttributeValue;
  String dateCreatedUtc;
  String dateUpdatedUtc;
  String dateCreated;
  String dateUpdated;
  int totalLikes;
  int totalComments;
  bool canDelete;
  String publishDateUtc;
  String publishDate;
  int likeStatus;
  bool isOwner;
  String checkin;
  double fromLat;
  double fromLng;
  List<dynamic> userCommentPublicModelList;
  int receiverId;
  int senderId;
  int taskId;

  TimeLineModel({
    this.id,
    this.ownerId,
    this.ownerEntityType,
    this.ownerImageUrl,
    this.ownerProfileUrl,
    this.ownerName,
    this.postTypeName,
    this.isSponsored,
    this.message,
    this.additionalAttributeValue,
    this.dateCreatedUtc,
    this.dateUpdatedUtc,
    this.dateCreated,
    this.dateUpdated,
    this.totalLikes,
    this.totalComments,
    this.canDelete,
    this.publishDateUtc,
    this.publishDate,
    this.likeStatus,
    this.isOwner,
    this.checkin,
    this.fromLat,
    this.fromLng,
    this.userCommentPublicModelList,
    this.receiverId,
    this.senderId,
    this.taskId,
  });

  factory TimeLineModel.fromJson(Map<String, dynamic> j) {
    var list_UserCommentPublicModelList = [];
    try {
      list_UserCommentPublicModelList =
          (j['UserCommentPublicModelList'] != null)
              ? j['UserCommentPublicModelList']
                  .map((i) => UserCommentPublicModelListModel.fromJson(i))
                  .toList()
              : [];
    } catch (e) {
      print(e.toString());
    }
    return TimeLineModel(
      id: j['Id'] ?? 0,
      ownerId: j['OwnerId'] ?? 0,
      ownerEntityType: j['OwnerEntityType'] ?? '',
      ownerImageUrl: j['OwnerImageUrl'] ?? '',
      ownerProfileUrl: j['OwnerProfileUrl'] ?? '',
      ownerName: j['OwnerName'] ?? '',
      postTypeName: j['PostTypeName'] ?? '',
      isSponsored: j['IsSponsored'] ?? false,
      message: j['Message'] ?? '',
      additionalAttributeValue: j['AdditionalAttributeValue'] ?? '',
      dateCreatedUtc: j['DateCreatedUtc'] ?? '',
      dateUpdatedUtc: j['DateUpdatedUtc'] ?? '',
      dateCreated: j['DateCreated'] ?? '',
      dateUpdated: j['DateUpdated'] ?? '',
      totalLikes: j['TotalLikes'] ?? 0,
      totalComments: j['TotalComments'] ?? 0,
      canDelete: j['CanDelete'] ?? false,
      publishDateUtc: j['PublishDateUtc'] ?? '',
      publishDate: j['PublishDate'] ?? '',
      likeStatus: j['LikeStatus'] ?? 0,
      isOwner: j['IsOwner'] ?? true,
      checkin: j['Checkin'] ?? '',
      fromLat: j['FromLat'] ?? 0.0,
      fromLng: j['FromLng'] ?? 0.0,
      userCommentPublicModelList: list_UserCommentPublicModelList,
      receiverId: j['ReceiverId'] ?? 0,
      senderId: j['SenderId'] ?? 0,
      taskId: j['TaskId'] ?? 0,
    );
  }

  Map<String, dynamic> toMap() => {
        'Id': id,
        'OwnerId': ownerId,
        'OwnerEntityType': ownerEntityType,
        'OwnerImageUrl': ownerImageUrl,
        'OwnerProfileUrl': ownerProfileUrl,
        'OwnerName': ownerName,
        'PostTypeName': postTypeName,
        'IsSponsored': isSponsored,
        'Message': message,
        'AdditionalAttributeValue': additionalAttributeValue,
        'DateCreatedUtc': dateCreatedUtc,
        'DateUpdatedUtc': dateUpdatedUtc,
        'DateCreated': dateCreated,
        'DateUpdated': dateUpdated,
        'TotalLikes': totalLikes,
        'TotalComments': totalComments,
        'CanDelete': canDelete,
        'PublishDateUtc': publishDateUtc,
        'PublishDate': publishDate,
        'LikeStatus': likeStatus,
        'IsOwner': isOwner,
        'Checkin': checkin,
        'FromLat': fromLat,
        'FromLng': fromLng,
        'UserCommentPublicModelList': userCommentPublicModelList,
        'ReceiverId': receiverId,
        'SenderId': senderId,
        'TaskId': taskId,
      };
}
