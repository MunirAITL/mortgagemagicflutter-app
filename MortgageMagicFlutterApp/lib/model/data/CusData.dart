class CusData {
  static final CusData _appData = new CusData._internal();

  Map cus;
  Map cus_loc;

  double discount;
  String discount_msg;
  Map fee;

  factory CusData() {
    return _appData;
  }
  CusData._internal();
}

final cusData = CusData();
