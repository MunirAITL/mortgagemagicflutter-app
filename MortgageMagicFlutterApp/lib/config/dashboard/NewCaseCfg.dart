import '../Server.dart';

class NewCaseCfg {
  static const int ALL = 901;
  static const int IN_PROGRESS = 902;
  static const int SUBMITTED = 903;
  static const int FMA_SUBMITTED = 904;
  static const int COMPLETED = 905;

  static const List<String> listSliderImages = [
    Server.DOMAIN + "/assets/img/slider/1.jpg",
    Server.DOMAIN + "/assets/img/slider/2.jpg",
    Server.DOMAIN + "/assets/img/slider/3.jpg",
  ];

  static const List<Map<String, dynamic>> listCreateNewCase = [
    {
      "title": "Residential Mortgage",
      "url": "assets/images/screens/home/new_case/ic_case_1.png",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "title": "Residential Remortgage",
      "url": "assets/images/screens/home/new_case/ic_case_2.png",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "title": "Second Charge - Residential",
      "url": "assets/images/screens/home/new_case/ic_case_3.png",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "title": "Buy to Let Mortgage",
      "url": "assets/images/screens/home/new_case/ic_case_4.png",
      "isOtherApplicant": true,
      "isSPV": true,
    },
    {
      "title": "Buy to Let Remortgage",
      "url": "assets/images/screens/home/new_case/ic_case_5.png",
      "isOtherApplicant": true,
      "isSPV": true,
    },
    {
      "title": "Business Lending",
      "url": "assets/images/screens/home/new_case/ic_case_6.png",
      "isOtherApplicant": true,
      "isSPV": true,
    },
    {
      "title": "Secound Charge Buy to Let & Commercial",
      "url": "assets/images/screens/home/new_case/ic_case_7.png",
      "isOtherApplicant": true,
      "isSPV": true,
    },
    {
      "title": "Commercial Mortgages or Loans",
      "url": "assets/images/screens/home/new_case/ic_case_8.png",
      "isOtherApplicant": true,
      "isSPV": true,
    },
    {
      "title": "Development Finance",
      "url": "assets/images/screens/home/new_case/ic_case_9.png",
      "isOtherApplicant": true,
      "isSPV": true,
    },
  ];
}
