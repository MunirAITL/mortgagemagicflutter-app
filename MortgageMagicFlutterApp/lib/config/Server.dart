class Server {
  static const bool isTest = true;

  //  api
  static const String BASE_URL = "https://app.mortgage-magic.co.uk";

  //  Login
  static const String LOGIN_URL = BASE_URL + "/api/authentication/login";

  //  Register
  static const String REG_URL = BASE_URL + "/api/authentication/register";

  //  New Case
  static const String NEWCASE_URL = BASE_URL +
      "/api/task/taskinformationbysearch" +
      "/get?SearchText=&Distance=50&Location=Dhaka,%20Bangladesh&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#";

  //  Post Case
  static const String POSTCASE_URL = BASE_URL + "/api/task/post";

  //  Notification
  static const String NOTI_URL =
      BASE_URL + "/api/notifications/get?userId=#userId#";

  //  Timeline
  static const String TIMELINE_URL = BASE_URL +
      "/api/timeline/get?IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&Count=#count#&CustomerId=#customerId#&Page=#page#&timeLineId=#timeLineId#";
  static const String TASKBIDDING_URL =
      BASE_URL + "/api/taskbidding/get?taskId=#taskId#";
  static const String TIMELINE_MESSAGES_URL = BASE_URL +
      "/api/timeline/get?Count=#count#&IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&count=#count#&customerId=#customerId#&page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_POST_URL = BASE_URL + "/api/timeline/post";

  //  More::Help->Support->Send Email  + Attachments
  static const String MEDIA_UPLOADFILES_URL =
      BASE_URL + "/api/media/uploadfiles";
  static const String RESOLUTION_URL = BASE_URL + "/api/resolution/post";

  //  More::Settings->Edit Profile
  static const String EDIT_PROFILE_URL = BASE_URL + "/api/users/put";

  //  WEBVIEW::
  //  Case Details WebView
  static const String CASEDETAILS_WEBVIEW_URL =
      BASE_URL + "/apps/about-me/#title#-#taskId#";
  static const String BASE_URL_NOTI_WEB = BASE_URL + "/apps/about-me";

  //  Misc
  static const String DOMAIN = "https://mortgage-magic.co.uk";
  static const String ABOUTUS_URL =
      "https://app.mortgage-magic.co.uk/apps/about-me/";
  static const String TC_URL = "https://trux24.com/privacy-policy";
  static const String PRIVACY_URL =
      "https://www.mortgagemagic.com/privacy-policy/";
}
