import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mypkg/classes/gps/GpsMgr.dart';
import 'package:mypkg/widgets/MyAlert.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:jiffy/jiffy.dart';

mixin Mixin {
  getW(context) {
    return MediaQuery.of(context).size.width;
  }

  getH(context) {
    return MediaQuery.of(context).size.height;
  }

  getWP(context, p) {
    return ResponsiveFlutter.of(context).wp(p);
  }

  getHP(context, p) {
    return ResponsiveFlutter.of(context).hp(p);
  }

  //  get user current location
  Future<Position> getLocation() async {
    Position pos;
    final isGPSPermission = await GpsMgr().requestLocationPermission();
    if (isGPSPermission) {
      pos = await Geolocator()
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    }
    return pos;
  }

  openUrl(context, url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      MyAlert(context, 'Could not launch $url', 0, null);
    }
  }

  openMap(context, double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      MyAlert(context, 'Could not launch ' + googleUrl, 0, null);
    }
  }

  Iterable<E> mapIndexed<E, T>(
      Iterable<T> items, E Function(int index, T item) f) sync* {
    var index = 0;
    for (final item in items) {
      yield f(index, item);
      index = index + 1;
    }
  }

  getTimeAgoTxt(dt) {
    try {
      final t = Jiffy(dt).fromNow();
      print(t);
      return t;
    } catch (e) {
      print(e.toString());
    }
    return dt;
  }

  void startLoading() {
    EasyLoading.show(status: 'loading...');
  }

  void stopLoading() {
    EasyLoading.dismiss();
  }

  void showToast({msg, which = 3}) {
    //  0 = error,
    //  1= success,
    //  2= info
    //  3= default toast
    switch (which) {
      case 0:
        EasyLoading.showError(msg);
        break;
      case 1:
        EasyLoading.showSuccess(msg);
        break;
      case 2:
        EasyLoading.showInfo('Useful Information.');
        break;
      default:
        EasyLoading.showToast(msg);
    }
  }

  void showSnake(GlobalKey<ScaffoldState> _scaffoldKey, String msg) {
    final snackBar = SnackBar(content: Text(msg));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  showAlert({context, msg, which = 2}) async {
    //  0 = error,
    //  1= success,
    //  2= info
    MyAlert(context, msg, which, null);
  }

  showAlertErr({context, title, msg, isServerErr = false}) async {
    MyAlert(
        context,
        (isServerErr)
            ? 'Please accept our apologies as our mobile apps will be back after completing some maintenance work on the web server.\n\nPlease try again later.'
            : 'Sorry, something went wrong. try again',
        2,
        null);
    if (msg != null) {
      //var geocode = await getGeoCode(true);
      sendMail(context, runtimeType.toString() + '-' + title, msg);
    }
  }

  showGPSError(context) {
    MyAlert(
        context,
        "Please enable your location from your device setting and allow location service while prompting here.",
        0,
        null);
  }

  log(str) {
    if (const String.fromEnvironment('DEBUG') != null) {
      print(str);
    }
  }

  log2(str) {
    if (const String.fromEnvironment('DEBUG') != null) {
      print(str);
    }
  }

  getRand(list) {
    final _random = new Random();
    return list[_random.nextInt(list.length)];
  }

  sendMail(context, subject, msg) async {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) async {});
  }
}
