class MyDefine {
  static const String EMAIL_URL =
      "https://mortgage-magic.co.uk/mob/mail/SendMail.php";
  static const String MISSING_IMG =
      "https://mortgage-magic.co.uk/api/content/media/default_avatar.png";
  static const EMAIL_KEY = "";

  static const CUS_CHATID_PREFIX = "Cus-";
  static const SP_CHATID_PREFIX = "SP-";

  //  ********************************************************  TEST

  static const test_lat = -36.826511;
  static const test_lng = 174.763753;
  static const test_geo_code = "uk";
}
