import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/config/MyDefine.dart';

class MyCacheImage extends StatelessWidget {
  final url;
  final imageProvider;
  final width;
  final height;
  final isCircle;

  MyCacheImage({
    this.url,
    this.imageProvider,
    this.width = 100.0,
    this.height = 100.0,
    this.isCircle = true,
  });
  @override
  Widget build(BuildContext context) {
    if (this.imageProvider == null) {
      return CachedNetworkImage(
        imageUrl: (url != null) ? url : MyDefine.MISSING_IMG,
        imageBuilder: (context, imageProvider) => Container(
          width: this.width,
          height: this.height,
          decoration: BoxDecoration(
            shape: (this.isCircle) ? BoxShape.circle : BoxShape.rectangle,
            image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
          ),
        ),
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      );
    } else {
      return Container(
        width: this.width,
        height: this.height,
        decoration: BoxDecoration(
          shape: (this.isCircle) ? BoxShape.circle : BoxShape.rectangle,
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
        child: (this.isCircle)
            ? CircleAvatar(
                radius: 30,
                backgroundColor: Colors.transparent,
                backgroundImage: imageProvider,
              )
            : Image(
                image: imageProvider,
              ),
      );
    }
  }
}
