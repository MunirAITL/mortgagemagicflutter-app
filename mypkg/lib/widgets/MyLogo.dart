import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyLogo extends StatelessWidget {
  final CircleColor;

  const MyLogo({Key key, @required this.CircleColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 220,
        child: Stack(
          children: <Widget>[
            Positioned(
                child: Container(
              child: Align(
                child: Container(
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: CircleColor),
                  width: 150,
                  height: 150,
                ),
              ),
              height: 154,
            )),
            Positioned(
              child: Container(
                  height: 154,
                  child: Align(
                    child: CircleAvatar(
                      backgroundColor: CircleColor,
                      radius: 50.0,
                      backgroundImage: AssetImage('assets/images/app_icon.png'),
                    ),
                  )),
            ),
            Positioned(
              width: MediaQuery.of(context).size.width * 0.15,
              height: MediaQuery.of(context).size.width * 0.15,
              bottom: MediaQuery.of(context).size.height * 0.046,
              right: MediaQuery.of(context).size.width * 0.22,
              child: Container(
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: CircleColor),
              ),
            ),
            Positioned(
              width: MediaQuery.of(context).size.width * 0.08,
              height: MediaQuery.of(context).size.width * 0.08,
              bottom: 0,
              right: MediaQuery.of(context).size.width * 0.32,
              child: Container(
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: CircleColor),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
