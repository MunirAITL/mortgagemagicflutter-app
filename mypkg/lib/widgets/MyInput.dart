import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyInput extends StatelessWidget {
  final Icon icon;
  final String prefixTxt;
  final String label;
  final String hint;
  final String err;
  final int len;
  final TextEditingController controller;
  final bool obsecure;
  final TextInputType kbType;

  MyInput({
    this.prefixTxt = '',
    this.icon,
    this.label,
    this.hint,
    this.err,
    this.len,
    this.controller,
    this.obsecure,
    this.kbType,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: TextFormField(
        validator: (value) {
          if (this.err != null) {
            if (value.isEmpty) {
              return this.err;
            }
          }
          return null;
        },
        controller: controller,
        obscureText: obsecure,
        keyboardType: kbType,
        maxLength: len,
        autocorrect: false,
        style: TextStyle(color: Colors.black, fontFamily: "Arial"),
        decoration: InputDecoration(
          counter: Offstage(),
          hintStyle: TextStyle(fontSize: 20, color: Colors.grey),
          hintText: hint,
          labelText: label,
          labelStyle: TextStyle(fontSize: 20, color: Colors.black),
          prefixText: prefixTxt,
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Colors.grey,
              width: 2,
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: BorderSide(
              color: Colors.black,
              width: 2,
            ),
          ),
          prefixIcon: Padding(
            child: IconTheme(
              data: IconThemeData(color: Colors.grey),
              child: icon,
            ),
            padding: EdgeInsets.only(left: 20, right: 10),
          ),
        ),
      ),
    );
  }
}
