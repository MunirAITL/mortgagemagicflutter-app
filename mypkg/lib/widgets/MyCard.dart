import 'package:flutter/material.dart';

class MyCard extends StatelessWidget {
  final Widget child;
  final Color color1;
  final Color color2;
  const MyCard({
    Key key,
    @required this.child,
    this.color1,
    this.color2,
  }) : super(key: key);
  /*@override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: 10,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.grey, width: 1),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 4),
          shape: BoxShape.rectangle,
          gradient: LinearGradient(
            begin: const Alignment(1, -0.5),
            end: const Alignment(0.2, 0.5),
            colors: [
              color1,
              color2,
            ],
          ),
        ),
        child: child,
      ),
    );
  }*/

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: 10,
      shape: RoundedRectangleBorder(
        side: BorderSide(color: Colors.grey, width: 1),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.white, width: 4),
          shape: BoxShape.rectangle,
        ),
        child: child,
      ),
    );
  }
}
