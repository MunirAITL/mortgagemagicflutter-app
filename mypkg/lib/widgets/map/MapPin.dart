import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mypkg/Mixin.dart';
import 'package:mypkg/config/MyConfig.dart';

class MapPin extends StatefulWidget {
  final head;
  final id;
  final cord;
  final addr;
  MapPin({
    Key key,
    this.head = "My Location",
    this.id,
    this.cord,
    this.addr,
  }) : super(key: key);

  @override
  State createState() => _MapPinState(head, id, cord, addr);
}

class _MapPinState extends State<MapPin> with Mixin {
  final head;
  final id;
  final cord;
  final addr;
  _MapPinState(
    this.head,
    this.id,
    this.cord,
    this.addr,
  );

  BitmapDescriptor pinLocationIcon;
  Set<Marker> _markers = {};
  Completer<GoogleMapController> _controller = Completer();
  @override
  void initState() {
    super.initState();
    BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.5),
            'assets/images/icons/pin.png')
        .then((onValue) {
      pinLocationIcon = onValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    //LatLng pinPosition = LatLng(37.3797536, -122.1017334);

    // these are the minimum required values to set
    // the camera position
    CameraPosition initialLocation =
        CameraPosition(zoom: 18, bearing: 30, target: cord);

    return Center(
      child: Container(
        child: Column(
          children: [
            Text(
              head,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                addr,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                ),
              ),
            ),
            Container(
              width: getW(context) * 90 / 100,
              height: getH(context) * 40 / 100,
              child: GoogleMap(
                myLocationEnabled: true,
                markers: _markers,
                initialCameraPosition: initialLocation,
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                  setState(() {
                    _markers.add(
                      Marker(
                        markerId: MarkerId(id.toString()),
                        position: cord,
                        icon: pinLocationIcon,
                        infoWindow: InfoWindow(title: addr, snippet: '*'),
                      ),
                    );
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
