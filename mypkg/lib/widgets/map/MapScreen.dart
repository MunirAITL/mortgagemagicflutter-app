import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mypkg/Mixin.dart';
import 'package:mypkg/widgets/map/MapLoc.dart';

class MapScreen extends StatefulWidget {
  final String url;
  final bool isCus;
  final String userID;
  LatLng cord;
  String addr;
  final geo_code;
  final double dis_radius_meter;

  MapScreen({
    Key key,
    @required this.url,
    @required this.isCus,
    @required this.userID,
    @required this.cord,
    @required this.addr,
    @required this.geo_code,
    @required this.dis_radius_meter,
  }) : super(key: key);

  @override
  State createState() => _MapState();
}

class _MapState extends State<MapScreen> with Mixin {
  var isUploading = false;

  @override
  void initState() {
    super.initState();

    updateAddress();
  }

  updateAddress() async {
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        Coordinates(widget.cord.latitude, widget.cord.longitude));
    widget.addr = addresses.first.addressLine;
    setState(() {});
  }

  void callback(LatLng cord, String address) {
    widget.addr = address;
    widget.cord = cord;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    (isUploading) ? startLoading() : stopLoading();

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text('My Location'),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
          centerTitle: true,
        ),
        body: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            MapLoc(
              geo_code: widget.geo_code,
              callback: callback,
              dis_radius_meter: widget.dis_radius_meter,
            ),
            Positioned(
              top: 10.0,
              left: 10.0,
              right: 10.0,
              child: Container(
                color: Colors.black.withOpacity(0.1),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Where are you looking for work?",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        widget.addr,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 60),
              width: 100,
              height: 150,
              child: Image.asset('assets/images/icons/pin.png'),
            ),
          ],
        ),
      ),
    );
  }
}
