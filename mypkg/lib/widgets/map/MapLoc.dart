import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mypkg/classes/MapFun.dart';
import 'package:mypkg/Mixin.dart';
import 'package:mypkg/model/data/MyAppData.dart';
import 'package:synchronized/extension.dart';
//  pan gesture on map: https://medium.com/flutter-community/flutter-deep-dive-gestures-c16203b3434f

class MapLoc extends StatelessWidget with Mixin {
  final geo_code;
  final dis_radius_meter;
  final void Function(LatLng, String) callback;

  static LatLng cord = null;

  MapLoc({
    @required this.geo_code,
    @required this.dis_radius_meter,
    @required this.callback,
  }) {
    if (cord == null) {
      cord = LatLng(myAppData.pos.latitude, myAppData.pos.longitude);
    }
  }

  Completer<GoogleMapController> _controller = Completer();

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  Set<Circle> drawCircle(LatLng latlng) {
    return Set.from([
      Circle(
        circleId: CircleId(latlng.toString()),
        center: latlng,
        radius: dis_radius_meter,
        fillColor: Colors.blue.withOpacity(0.2),
        strokeColor: Colors.blue.withOpacity(0.25),
      )
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return RawGestureDetector(
      gestures: {
        AllowMultipleGestureRecognizer: GestureRecognizerFactoryWithHandlers<
            AllowMultipleGestureRecognizer>(
          () => AllowMultipleGestureRecognizer(), //constructor
          (AllowMultipleGestureRecognizer instance) {
            //initializer
            instance.onEnd = (v) {
              //log(v);
              Future.delayed(Duration.zero, () {
                updateGeo();
              });
            };
          },
        )
      },
      child: GoogleMap(
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        zoomControlsEnabled: false,
        zoomGesturesEnabled: true,
        scrollGesturesEnabled: true,
        compassEnabled: true,
        rotateGesturesEnabled: true,
        mapToolbarEnabled: true,
        tiltGesturesEnabled: true,
        mapType: MapType.normal,
        onMapCreated: _onMapCreated,
        circles: drawCircle(LatLng(cord.latitude, cord.longitude)),
        initialCameraPosition: CameraPosition(
          target: cord,
          zoom: 12.0,
        ),
        gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
          new Factory<OneSequenceGestureRecognizer>(
            () => new EagerGestureRecognizer(),
          ),
        ].toSet(),
        onCameraMove: (CameraPosition cameraPosition) =>
            _handleCameraMove(cameraPosition),
      ),
    );
  }

  _handleCameraMove(CameraPosition cameraPosition) {
    cord = cameraPosition.target;
    log(cord.toString());
  }

  Future updateGeo() {
    // Lock at the instance level
    return synchronized(() async {
      // ...uninterrupted action
      try {
        callback(
            cord,
            await MapFun().getAddrByCord(
                cord.latitude.toString() + ',' + cord.longitude.toString()));
      } catch (e) {}
    });
  }
}

class AllowMultipleGestureRecognizer extends PanGestureRecognizer {
  @override
  void rejectGesture(int pointer) {
    acceptGesture(pointer);
  }
}
