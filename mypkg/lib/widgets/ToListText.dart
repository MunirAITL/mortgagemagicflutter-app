import 'package:flutter/material.dart';

class ToListText extends StatelessWidget {
  final commaSeperatedString;
  final colorTxt;
  final sizeTxt;
  const ToListText({
    Key key,
    @required this.commaSeperatedString,
    this.colorTxt = Colors.white,
    this.sizeTxt = 20.0,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: commaSeperatedString
            .map<Widget>(
              (item) => new Text(
                item,
                style: TextStyle(
                  fontSize: sizeTxt,
                  fontWeight: FontWeight.bold,
                  color: colorTxt,
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
