import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';
import 'package:mypkg/widgets/MyCacheImage.dart';

class MyHeaderTitle extends StatelessWidget with Mixin {
  final pic;
  final title;
  final subtitle;
  final primaryColor;
  final primaryColor2;

  const MyHeaderTitle({
    Key key,
    @required this.pic,
    @required this.title,
    @required this.subtitle,
    @required this.primaryColor,
    this.primaryColor2 = Colors.blueAccent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //SizedBox(height: height * 0.2),
        Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.white, width: 4),
            shape: BoxShape.rectangle,
            gradient: LinearGradient(
              begin: const Alignment(0.7, -0.5),
              end: const Alignment(0.6, 0.5),
              colors: [
                primaryColor2,
                primaryColor,
              ],
            ),
          ),
          height: getH(context) * 0.4,
          child: Stack(
            children: <Widget>[
              Container(
                height: getH(context) * 0.2,
                width: double.infinity,
                child: MyCacheImage(url: pic),
              ),
              Positioned(
                bottom: 0,
                left: getW(context) * 0.05,
                top: getH(context) * 0.20,
                child: Container(
                    color: Colors.white,
                    width: getW(context) * 0.95,
                    //height: 50,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              title,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20.0,
                                fontFamily: 'Georgia',
                              ),
                            ),
                            Text(
                              subtitle,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 17.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
