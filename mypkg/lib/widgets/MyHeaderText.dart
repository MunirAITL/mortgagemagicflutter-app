import 'package:flutter/material.dart';

class MyHeaderText extends StatelessWidget {
  final String text;
  final double textSize;
  const MyHeaderText({Key key, @required this.text, this.textSize = 20})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
          height: 2,
          color: Colors.black54,
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                Container(
                  width: (text.length * textSize).toDouble(),
                  margin: new EdgeInsetsDirectional.only(start: 1.0, end: 1.0),
                  height: 2.0,
                  color: Colors.black,
                ),
                Text(
                  text,
                  textAlign: TextAlign.left,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: textSize,
                      fontFamily: 'Georgia'),
                ),
              ],
            ),
          ),
        ),
        SizedBox(height: 10)
      ],
    );
  }
}
