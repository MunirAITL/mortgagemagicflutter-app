import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class MyTextHeading extends StatelessWidget with Mixin {
  final String text;
  final double size;
  final Color color;
  const MyTextHeading({
    Key key,
    @required this.text,
    this.size = 18,
    this.color = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getW(context),
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: BorderedText(
            strokeWidth: 2.0,
            strokeColor: color,
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(
                  decoration: TextDecoration.none,
                  decorationColor: Colors.white,
                  fontSize: size),
            ),
          ),
        ),
      ),
    );
  }
}
