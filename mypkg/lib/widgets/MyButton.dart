import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final String text;
  final Color bgColor, borderColor, splashColor, highlightColor, textColor;
  final Function fun;
  final isOutline;
  final isCloseBtn;
  MyButton({
    @required this.text,
    this.isOutline = false,
    this.isCloseBtn = false,
    this.bgColor = Colors.blue,
    this.borderColor = Colors.black,
    this.splashColor = Colors.yellow,
    this.highlightColor = Colors.green,
    this.textColor = Colors.black,
    @required this.fun,
  });

  @override
  Widget build(BuildContext context) {
    return (!isOutline)
        ? getButton(text, textColor, bgColor, fun)
        : (isCloseBtn)
            ? OutlineButton(
                child: Icon(
                  Icons.close,
                  size: 50,
                  color: Colors.grey,
                ),
                onPressed: () => fun(),
              )
            : OutlineButton(
                highlightElevation: 1.0,
                splashColor: splashColor,
                highlightColor: highlightColor,
                color: Theme.of(context).primaryColor,
                child: new Text(text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        backgroundColor: Colors.transparent,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: textColor)),
                onPressed: () => fun(),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0),
                ),
                borderSide: BorderSide(color: borderColor),
              );
  }

  static Widget getButton(txt, textColor, bgColor, fun) {
    return new RaisedButton(
      onPressed: () => fun(),
      color: bgColor,
      splashColor: Colors.deepPurple,
      shape: RoundedRectangleBorder(
        borderRadius: const BorderRadius.all(const Radius.circular(100.0)),
      ),
      //padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 80.0),
      textColor: textColor,
      child: new Text(
        txt,
        textAlign: TextAlign.center,
        style: const TextStyle(fontSize: 17.0, fontWeight: FontWeight.bold),
      ),
    );
  }
}
