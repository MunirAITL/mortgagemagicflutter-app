import 'package:flutter/material.dart';
import 'MyCacheImage.dart';

class MyHeaderRate extends StatelessWidget {
  final primaryColor;
  final primaryColor2;
  final pic;
  final name;
  final rate;
  const MyHeaderRate({
    Key key,
    @required this.pic,
    @required this.name,
    @required this.rate,
    @required this.primaryColor,
    this.primaryColor2 = Colors.blueAccent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white, width: 4),
        shape: BoxShape.rectangle,
        gradient: LinearGradient(
          begin: const Alignment(0.7, -0.5),
          end: const Alignment(0.6, 0.5),
          colors: [
            primaryColor2,
            primaryColor,
          ],
        ),
      ),
      child: DrawerHeader(
          margin: EdgeInsets.zero,
          padding: EdgeInsets.zero,
          child: Stack(children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Column(
                children: [
                  MyCacheImage(
                    url: pic,
                    height: MediaQuery.of(context).size.height * .1,
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      name,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment
                          .center, //Center Row contents horizontally,
                      crossAxisAlignment: CrossAxisAlignment
                          .center, //Center Row contents vertically,
                      children: <Widget>[
                        Icon(Icons.star, color: Colors.yellow, size: 30),
                        Text(
                          rate.toString(),
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ])),
    );
  }
}
