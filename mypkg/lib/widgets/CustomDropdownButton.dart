import 'package:flutter/material.dart';

class CustomDropDownButton extends StatelessWidget {
  final List<String> list;
  final val;
  final hint;
  final Function(String val) fun;
  const CustomDropDownButton({
    Key key,
    @required this.list,
    @required this.val,
    @required this.hint,
    @required this.fun,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
          color: Colors.grey, borderRadius: BorderRadius.circular(10)),
      // dropdown below..
      child: DropdownButton<String>(
        isExpanded: true,
        dropdownColor: Colors.white,
        focusColor: Colors.white,
        value: val ?? list[0],
        hint: new Text(
          hint,
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        icon: Icon(
          Icons.arrow_downward,
          color: Colors.white,
        ),
        iconSize: 30,
        elevation: 16,
        style: TextStyle(
          color: Colors.white,
          fontSize: 20,
        ),
        underline: SizedBox(),
        onChanged: (String newValue) {
          fun(newValue);
        },
        items: list.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
              style: TextStyle(color: Colors.black),
              overflow: TextOverflow.ellipsis,
            ),
          );
        }).toList(),
      ),
    );
  }
}
