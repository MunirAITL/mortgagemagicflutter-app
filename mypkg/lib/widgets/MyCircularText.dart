import 'package:flutter/material.dart';
import 'package:flutter_circular_text/circular_text.dart';

class MyCircularText extends StatelessWidget {
  final txt1;
  final txt2;
  final radius;
  final colorBG;
  final colorTxt;
  const MyCircularText(
      {Key key,
      this.txt1,
      this.txt2,
      this.radius = 100.0,
      this.colorBG = Colors.transparent,
      this.colorTxt = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CircularText(
        children: [
          TextItem(
            text: Text(
              txt1,
              style: TextStyle(
                fontSize: 30,
                color: colorTxt,
                fontWeight: FontWeight.bold,
              ),
            ),
            space: 12,
            startAngle: -90,
            startAngleAlignment: StartAngleAlignment.center,
            direction: CircularTextDirection.clockwise,
          ),
          TextItem(
            text: Text(
              txt2,
              style: TextStyle(
                fontSize: 20,
                color: colorTxt,
                fontWeight: FontWeight.bold,
              ),
            ),
            space: 10,
            startAngle: 90,
            startAngleAlignment: StartAngleAlignment.center,
            direction: CircularTextDirection.anticlockwise,
          ),
        ],
        radius: radius,
        position: CircularTextPosition.inside,
        //backgroundPaint: Paint()..color = Colors.grey.shade200,
        backgroundPaint: Paint()..color = colorBG,
      ),
    );
  }
}
