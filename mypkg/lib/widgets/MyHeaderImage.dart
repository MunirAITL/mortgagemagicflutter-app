import 'package:flutter/material.dart';
import 'package:mypkg/Mixin.dart';

class MyHeaderImage extends StatelessWidget with Mixin {
  final String bgImage;
  final String title;
  const MyHeaderImage({
    Key key,
    @required this.bgImage,
    @required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //SizedBox(height: height * 0.2),
        Container(
          //color: Colors.transparency,
          height: getH(context) * 0.35,
          child: Stack(
            children: <Widget>[
              Container(
                height: getH(context) * 0.30,
                width: double.infinity,
                child: Image.asset(
                  'assets/images/bg/' + bgImage,
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                bottom: 0,
                left: getW(context) * 0.1,
                top: getH(context) * 0.25,
                child: Container(
                    color: Colors.white,
                    width: getW(context) * 0.90,
                    //height: 10,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          title,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontFamily: 'Georgia',
                          ),
                        ),
                      ),
                    )),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
