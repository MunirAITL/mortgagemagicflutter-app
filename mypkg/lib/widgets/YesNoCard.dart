import 'package:flutter/material.dart';

class YesNoCard extends StatelessWidget {
  final title;
  final arg;

  const YesNoCard({Key key, @required this.title, @required this.arg})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: Text(
                  title,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            (int.parse(arg) == 1)
                ? Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: Image.asset(
                      'assets/images/icons/yes.png',
                      height: 35,
                      width: 35,
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: Image.asset(
                      'assets/images/icons/no.png',
                      height: 35,
                      width: 30,
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
