import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:mypkg/model/data/MyAppData.dart';
//  https://console.firebase.google.com/project/mortgagemagicflutterapp/notification

class PushNotificationsManager {
  PushNotificationsManager._();

  factory PushNotificationsManager() => _instance;

  static final PushNotificationsManager _instance =
      PushNotificationsManager._();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  Future<FirebaseMessaging> init() async {
    if (!_initialized) {
      // For iOS request permission first.
      _firebaseMessaging.requestNotificationPermissions();
      _firebaseMessaging.configure();

      // For testing purposes print the Firebase Messaging token
      myAppData.token = await _firebaseMessaging.getToken();
      //print("FirebaseMessaging token: $appData.token");

      _initialized = true;
      //callback(_firebaseMessaging);
      return _firebaseMessaging;
    }

    /*_firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> message) async {
        //
        print(message);
        callback(0, message);
      },
      onResume: (Map<String, dynamic> message) async {
        //
        callback(1, message);
      },
      onMessage: (Map<String, dynamic> message) async {
        //
        callback(2, message);
      },
    );
    //GETTING TOKEN FOR TESTING MANUALY
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
    });*/
  }
}
