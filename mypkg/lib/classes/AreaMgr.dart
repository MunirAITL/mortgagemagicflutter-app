import 'dart:math' as Math;
import 'package:google_maps_flutter/google_maps_flutter.dart';

class AreaMgr {
  //  https://stackoverflow.com/questions/28838287/calculate-the-area-of-a-polygon-drawn-on-google-maps-in-an-android-application
  static final double EARTH_RADIUS = 6371000; // meters

  static double calculateAreaOfGPSPolygonOnEarthInSquareMeters(
      final List<LatLng> locations) {
    return calculateAreaOfGPSPolygonOnSphereInSquareMeters(
        locations, EARTH_RADIUS);
  }

  static double calculateAreaOfGPSPolygonOnSphereInSquareMeters(
      final List<LatLng> locations, final double radius) {
    if (locations.length < 3) {
      return 0;
    }

    final double diameter = radius * 2;
    final double circumference = diameter * Math.pi;
    final List<double> listY = [];
    final List<double> listX = [];
    final List<double> listArea = [];
    // calculate segment x and y in degrees for each point
    final double latitudeRef = locations[0].latitude;
    final double longitudeRef = locations[1].longitude;
    for (int i = 1; i < locations.length; i++) {
      final double latitude = locations[i].latitude;
      final double longitude = locations[i].longitude;
      listY.add(calculateYSegment(latitudeRef, latitude, circumference));
      listX.add(
          calculateXSegment(longitudeRef, longitude, latitude, circumference));
    }

    // calculate areas for each triangle segment
    for (int i = 1; i < listX.length; i++) {
      final double x1 = listX[i - 1];
      final double y1 = listY[i - 1];
      final double x2 = listX[i];
      final double y2 = listY[i];
      listArea.add(calculateAreaInSquareMeters(x1, x2, y1, y2));
    }

    // sum areas of all triangle segments
    double areasSum = 0;
    for (final double area in listArea) {
      areasSum = areasSum + area;
    }

    // get abolute value of area, it can't be negative
    return areasSum.abs(); // Math.sqrt(areasSum * areasSum);
  }

  static double calculateAreaInSquareMeters(
      final double x1, final double x2, final double y1, final double y2) {
    return (y1 * x2 - x1 * y2) / 2;
  }

  static double calculateYSegment(final double latitudeRef,
      final double latitude, final double circumference) {
    return (latitude - latitudeRef) * circumference / 360.0;
  }

  static double calculateXSegment(
      final double longitudeRef,
      final double longitude,
      final double latitude,
      final double circumference) {
    return (longitude - longitudeRef) *
        circumference *
        Math.cos(_convertToRadian(latitude)) /
        360.0;
  }

/*
  static double calculatePolygonArea(List coordinates) {
    double area = 0;

    if (coordinates.length > 2) {
      for (var i = 0; i < coordinates.length - 1; i++) {
        var p1 = coordinates[i];
        var p2 = coordinates[i + 1];
        area += convertToRadian(p2.longitude - p1.longitude) *
            (2 +
                Math.sin(convertToRadian(p1.latitude)) +
                Math.sin(convertToRadian(p2.latitude)));
      }

      //area = area * 6378137 * 6378137 / 2;
      area = area * 6378137 / 2;
    }

    final acreArea = area.abs() * 0.000247105; //sq meters to Acres
    print(area.toString());

    //  sq feet = US
    //  sq meter = NZ
    return acreArea * double.parse(appData.country["area_ratio"]);
    //return area.abs() * 0.000247105; //sq meters to Acres
  }
*/
  static double _convertToRadian(double input) {
    return input * Math.pi / 180;
  }
}
