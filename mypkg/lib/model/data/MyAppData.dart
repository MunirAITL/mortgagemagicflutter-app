import 'package:geolocator/geolocator.dart';

class MyAppData {
  static final MyAppData _myAppData = new MyAppData._internal();
  String token;
  Position pos;

  factory MyAppData() {
    return _myAppData;
  }
  MyAppData._internal();
}

final myAppData = MyAppData();
