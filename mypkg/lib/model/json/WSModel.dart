class WSModel {
  // Id will be gotten from the database.
  // It's automatically generated & unique for every stored Fruit.
  int id;

  final bool error;
  final String reason;
  final Map<dynamic, dynamic> json;

  WSModel({this.error, this.reason, this.json});

  Map<dynamic, dynamic> toList() {
    return json;
  }

  static WSModel fromMap(Map<String, dynamic> map) {
    return WSModel(
      error: map['error'] as bool,
      reason: map['reason'] as String,
      json: map['json'] as Map<dynamic, dynamic>,
    );
  }
}
