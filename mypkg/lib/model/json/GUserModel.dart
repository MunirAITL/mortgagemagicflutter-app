class GUserModel {
  final String displayName;
  final String email;
  final String phoneNumber;
  final String photoUrl;
  final String gender;
  GUserModel({
    this.displayName,
    this.email,
    this.phoneNumber,
    this.photoUrl,
    this.gender,
  });
}
